package gameState.levelSelection;
import main.Main;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class LevelIcon {

	private String title;
	private boolean locked;
	private Image level = null;
	private Image lock = null;
	private int x, y, width, height;
	
	public LevelIcon(String title, boolean locked, Image level, int x, int y, int width, int height) throws SlickException {
		this.title = title;
		this.locked = locked;
		this.level = level;
		lock = Main.resources.getLevelIcons().get(0);
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public String getTitle() {
		return title;
	}
	
	public boolean getLockStatus() {
		return locked;
	}
	
	public Image getLevelImage() {
		return level;
	}
	
	public Image getLockImage() {
		return lock;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
}