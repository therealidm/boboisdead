package gameState.levelSelection;
import java.util.ArrayList;

import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class LevelSelection extends BasicGameState {
	
	private int id;
	private LevelSet levels = null;
	private Input input;
	
	private Rectangle backRect = null;
	private Rectangle joyRect = null;
	private ArrayList<Rectangle> rects = null;
	
	public LevelSelection(int id) {
		this.id = id;
	}
	
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		input = container.getInput();
		
		backRect = new Rectangle(80, 470, 207, 72);
		joyRect = new Rectangle(200, 20, 75, 50);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
	    
	    if(backRect.contains(mouseX, container.getHeight()-mouseY)) {
	         if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	 Main.resources.getSoundEffects().get(0).play();
	        	 sbg.enterState(Main.menu);
	         }
	    }
	    
	    if(joyRect.contains(mouseX, container.getHeight()-mouseY)) {
	         if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	 Main.resources.getSoundEffects().get(0).play();
	        	 Main.joystick = !Main.joystick;
	         }
	    }
	    
	    for(int i=0; i<Main.levelsCount; i++) {
	    	if((rects.get(i).contains(mouseX, container.getHeight()-mouseY)) && (levels.getLevelIcon(i).getLockStatus() == true)) {
	    		if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	    			Main.resources.getSoundEffects().get(0).play();
	    			Main.levelSelected = i + 1;
	    			sbg.enterState(Main.storyline, new FadeOutTransition(), new FadeInTransition());
	    		}
	    	}
	    }
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(0).draw(930, 50, 250, 50);
	    Main.resources.getMenuIcons().get(5).draw(80, 470);
	    if(Main.joystick)
	    	Main.resources.getMenuIcons().get(14).draw(200, 20, 75, 50);
	    else
	    	Main.resources.getMenuIcons().get(15).draw(200, 20, 75, 50);
	    	
	    g.drawString("Username: " + Main.profile1.getName(), 300, 20);
	    g.drawString("Kills: " + Main.profile1.getEnemiesKilled(), 300, 40);
	    if(Main.multiplayer) {
	    	g.drawString("Username: " + Main.profile2.getName(), 550, 20);
		    g.drawString("Kills: " + Main.profile2.getEnemiesKilled(), 550, 40);
	    }
	    
	    for(int i=0; i<Main.levelsCount; i++) {
	    	g.drawString(levels.getLevelIcon(i).getTitle(), levels.getLevelIcon(i).getX(), levels.getLevelIcon(i).getY() - 20);
	    	if(levels.getLevelIcon(i).getLockStatus() == false)
	    		levels.getLevelIcon(i).getLockImage().draw(levels.getLevelIcon(i).getX(), levels.getLevelIcon(i).getY(), levels.getLevelIcon(i).getWidth(), levels.getLevelIcon(i).getHeight());
	    	else
	    		levels.getLevelIcon(i).getLevelImage().draw(levels.getLevelIcon(i).getX(), levels.getLevelIcon(i).getY(), levels.getLevelIcon(i).getWidth(), levels.getLevelIcon(i).getHeight());
	    }
	}

	public int getID() {
		return id;
	}
	
	public void enter(GameContainer container, StateBasedGame sbg) throws SlickException {
		levels = new LevelSet();
		rects = new ArrayList<Rectangle>();
		rects.add(new Rectangle(levels.getLevelIcon(0).getX(), levels.getLevelIcon(0).getY(), levels.getLevelIcon(0).getWidth(), levels.getLevelIcon(0).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(1).getX(), levels.getLevelIcon(1).getY(), levels.getLevelIcon(1).getWidth(), levels.getLevelIcon(1).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(2).getX(), levels.getLevelIcon(2).getY(), levels.getLevelIcon(2).getWidth(), levels.getLevelIcon(2).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(3).getX(), levels.getLevelIcon(3).getY(), levels.getLevelIcon(3).getWidth(), levels.getLevelIcon(3).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(4).getX(), levels.getLevelIcon(4).getY(), levels.getLevelIcon(4).getWidth(), levels.getLevelIcon(4).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(5).getX(), levels.getLevelIcon(5).getY(), levels.getLevelIcon(5).getWidth(), levels.getLevelIcon(5).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(6).getX(), levels.getLevelIcon(6).getY(), levels.getLevelIcon(6).getWidth(), levels.getLevelIcon(6).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(7).getX(), levels.getLevelIcon(7).getY(), levels.getLevelIcon(7).getWidth(), levels.getLevelIcon(7).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(8).getX(), levels.getLevelIcon(8).getY(), levels.getLevelIcon(8).getWidth(), levels.getLevelIcon(8).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(9).getX(), levels.getLevelIcon(9).getY(), levels.getLevelIcon(9).getWidth(), levels.getLevelIcon(9).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(10).getX(), levels.getLevelIcon(10).getY(), levels.getLevelIcon(10).getWidth(), levels.getLevelIcon(10).getHeight()));
		rects.add(new Rectangle(levels.getLevelIcon(11).getX(), levels.getLevelIcon(11).getY(), levels.getLevelIcon(11).getWidth(), levels.getLevelIcon(11).getHeight()));
		
		Main.levelSelected = 0;
		if(!Main.resources.getSoundtrack().get(0).playing())
			Main.resources.getSoundtrack().get(0).loop();
	}
	
	public void leave(GameContainer container, StateBasedGame sbg) {
		if(Main.levelSelected != 0)
			Main.resources.getSoundtrack().get(0).stop();
	}
}