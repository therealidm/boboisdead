package gameState.levelSelection;
import java.util.ArrayList;

import main.Main;

import org.newdawn.slick.SlickException;

public class LevelSet {
	
	private ArrayList<LevelIcon> levels = null;
	
	public LevelSet() throws SlickException {
		levels = new ArrayList<LevelIcon>();
		
		if(!Main.multiplayer) {
			levels.add(new LevelIcon("The Beginning", Main.profile1.getLvl1(), Main.resources.getLevelIcons().get(1), 45, 50, 100, 100));
			levels.add(new LevelIcon("Anubi Strikes Back", Main.profile1.getLvl2(), Main.resources.getLevelIcons().get(2), 150, 200, 100, 100));
			levels.add(new LevelIcon("Burn Aliens, BURN!", Main.profile1.getLvl3(), Main.resources.getLevelIcons().get(3), 250, 350, 100, 100));
			levels.add(new LevelIcon("In The Name Of God", Main.profile1.getLvl4(), Main.resources.getLevelIcons().get(4), 410, 450, 100, 100));
			levels.add(new LevelIcon("Road To Machu Picchu", Main.profile1.getLvl5(), Main.resources.getLevelIcons().get(5), 600, 490, 100, 100));
			levels.add(new LevelIcon("Attack On Canary Islands", Main.profile1.getLvl6(), Main.resources.getLevelIcons().get(6), 820, 470, 100, 100));
			levels.add(new LevelIcon("Heads Will Roll", Main.profile1.getLvl7(), Main.resources.getLevelIcons().get(7), 980, 370, 100, 100));
			levels.add(new LevelIcon("The Shadow Of Boboli", Main.profile1.getLvl8(), Main.resources.getLevelIcons().get(8), 945, 200, 100, 100));
			levels.add(new LevelIcon("Badass Rasputin", Main.profile1.getLvl9(), Main.resources.getLevelIcons().get(9), 720, 120, 100, 100));
			levels.add(new LevelIcon("Nazi Slaughter", Main.profile1.getLvl10(), Main.resources.getLevelIcons().get(10), 455, 140, 100, 100));
			levels.add(new LevelIcon("Vietnam Madness", Main.profile1.getLvl11(), Main.resources.getLevelIcons().get(11), 475, 300, 100, 100));
			levels.add(new LevelIcon("The End...", Main.profile1.getLvl12(), Main.resources.getLevelIcons().get(12), 720, 310, 100, 100));
		} else {	
			levels.add(new LevelIcon("The Beginning", Main.profile1.getLvl1() && Main.profile2.getLvl1(), Main.resources.getLevelIcons().get(1), 45, 50, 100, 100));
			levels.add(new LevelIcon("Anubi Strikes Back", Main.profile1.getLvl2() && Main.profile2.getLvl2(), Main.resources.getLevelIcons().get(2), 150, 200, 100, 100));
			levels.add(new LevelIcon("Burn Aliens, BURN!", Main.profile1.getLvl3() && Main.profile2.getLvl3(), Main.resources.getLevelIcons().get(3), 250, 350, 100, 100));
			levels.add(new LevelIcon("In The Name Of God", Main.profile1.getLvl4() && Main.profile2.getLvl4(), Main.resources.getLevelIcons().get(4), 410, 450, 100, 100));
			levels.add(new LevelIcon("Road To Machu Picchu", Main.profile1.getLvl5() && Main.profile2.getLvl5(), Main.resources.getLevelIcons().get(5), 600, 490, 100, 100));
			levels.add(new LevelIcon("Attack On Canary Islands", Main.profile1.getLvl6() && Main.profile2.getLvl6(), Main.resources.getLevelIcons().get(6), 820, 470, 100, 100));
			levels.add(new LevelIcon("Heads Will Roll", Main.profile1.getLvl7() && Main.profile2.getLvl7(), Main.resources.getLevelIcons().get(7), 980, 370, 100, 100));
			levels.add(new LevelIcon("The Shadow Of Boboli", Main.profile1.getLvl8() && Main.profile2.getLvl8(), Main.resources.getLevelIcons().get(8), 945, 200, 100, 100));
			levels.add(new LevelIcon("Badass Rasputin", Main.profile1.getLvl9() && Main.profile2.getLvl9(), Main.resources.getLevelIcons().get(9), 720, 120, 100, 100));
			levels.add(new LevelIcon("Nazi Slaughter", Main.profile1.getLvl10() && Main.profile2.getLvl10(), Main.resources.getLevelIcons().get(10), 455, 140, 100, 100));
			levels.add(new LevelIcon("Vietnam Madness", Main.profile1.getLvl11() && Main.profile2.getLvl11(), Main.resources.getLevelIcons().get(11), 475, 300, 100, 100));
			levels.add(new LevelIcon("The End...", Main.profile1.getLvl12() && Main.profile2.getLvl12(), Main.resources.getLevelIcons().get(12), 720, 310, 100, 100));
		}
	}

	public LevelIcon getLevelIcon(int i) {
		return levels.get(i);
	}
}