package gameState;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Storyline extends BasicGameState {
	
	private int id;
	private Input input;
	private float scale =(float) 1.171875;
	
	private Rectangle contRect = null;
	private ArrayList<String> fileLines = null;
	
	public Storyline(int id) {
		this.id = id;
	}

	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		input = container.getInput();
		contRect = new Rectangle(893,450,207,72);
		fileLines = new ArrayList<String>();
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
		int mouseY = Mouse.getY();
    
		if(contRect.contains(mouseX, container.getHeight()-mouseY)) {
			if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
				Main.resources.getSoundEffects().get(0).play();
				sbg.enterState(Main.levelState);
			}
		}
	}

	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		g.scale(this.scale, this.scale);
		Main.resources.getBackgrounds().get(Main.levelSelected).draw(0, 0);
	    Main.resources.getMenuIcons().get(6).draw(893/scale, 450/scale);
	    drawTitle(g);
	    drawStory(g);
	}
	
	public void drawTitle(Graphics g) {
		switch(Main.levelSelected) {
			case 1: g.drawString("The Beginning", 50, 50);
					break;
			case 2: g.drawString("Anubi Strikes Back", 50, 50);
					break;
			case 3: g.drawString("Burn Aliens, BURN!", 50, 50);
					break;
			case 4: g.drawString("In The Name Of God", 50, 50);
					break;
			case 5: g.drawString("Road To Machu Picchu", 50, 50);
					break;
			case 6: g.drawString("Attack On Canary Islands", 50, 50);
					break;
			case 7: g.drawString("Heads Will Roll", 50, 50);
					break;
			case 8: g.drawString("The Shadow Of Boboli", 50, 50);
					break;
			case 9: g.drawString("Badass Rasputin", 50, 50);
					break;
			case 10: g.drawString("Nazi Slaughter", 50, 50);
					break;
			case 11: g.drawString("Vietnam Madness", 50, 50);
					break;
			case 12: g.drawString("The End...", 50, 50);
					break;
			default:g.drawString("Empty Level", 50, 50);
					break;	
		}
	}
	
	public void drawStory(Graphics g) {
		for(int i = 0; i<fileLines.size(); i++)
			g.drawString(fileLines.get(i), 100, 100+20*i);
	}
	
	private void getLinesFromFile() {
		
		try {
			
			FileInputStream f = new FileInputStream(Main.resources.getStoryFiles().get(Main.levelSelected));
			BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(f)));
			String line = new String();
			
			while((line = br.readLine()) != null) {
				fileLines.add(new String(line));
			}
			
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void enter(GameContainer container, StateBasedGame sbg) throws SlickException {
		Main.resources.getSoundtrack().get(Main.levelSelected).loop();
		getLinesFromFile();
	}
	
	public void leave(GameContainer container, StateBasedGame sbg) throws SlickException {
		fileLines.clear();
	}
	
	public int getID() {
		return id;
	}
}