package gameState.options;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Bobopedia extends BasicGameState {
	
	private int id;
	private Input input;
	
	private Rectangle backRect = null;
	private ArrayList<Rectangle> rects = null;
		
	public Bobopedia(int id) {
		this.id = id;
	}

	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		backRect = new Rectangle(80, 470, 207, 72);
		rects = new ArrayList<Rectangle>();
		rects.add(new Rectangle(45, 50, 100, 100));
		rects.add(new Rectangle(150, 200, 100, 100));
		rects.add(new Rectangle(250, 350, 100, 100));
		rects.add(new Rectangle(410, 450, 100, 100));
		rects.add(new Rectangle(600, 490, 100, 100));
		rects.add(new Rectangle(820, 470, 100, 100));
		rects.add(new Rectangle(980, 370, 100, 100));
		rects.add(new Rectangle(945, 200, 100, 100));
		rects.add(new Rectangle(720, 120, 100, 100));
		rects.add(new Rectangle(455, 140, 100, 100));
		rects.add(new Rectangle(475, 300, 100, 100));
		rects.add(new Rectangle(720, 310, 100, 100));
		
		input = container.getInput();
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
	    
	    for(int i=0; i<Main.levelsCount; i++) {
	    	if(rects.get(i).contains(mouseX, container.getHeight()-mouseY)) {
	    		if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	    			Main.resources.getSoundEffects().get(0).play();
	    			openWikipedia(rects.get(i));
	    		}
	    	} 
	    }
	    
	    if(backRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	        	sbg.enterState(Main.options);
	        }
	    }
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
		Main.resources.getMenuIcons().get(0).draw(930, 50, 250, 50);
	    Main.resources.getMenuIcons().get(5).draw(80, 470);
	    
	    drawLvl(g, "Prehistory", Main.resources.getWikiIcons().get(1), rects.get(0));
	    drawLvl(g, "Egypt", Main.resources.getWikiIcons().get(2), rects.get(1));
	    drawLvl(g, "Rome", Main.resources.getWikiIcons().get(3), rects.get(2));
	    drawLvl(g, "Crusade", Main.resources.getWikiIcons().get(4), rects.get(3));
	    drawLvl(g, "Conquistadores", Main.resources.getWikiIcons().get(5), rects.get(4));
	    drawLvl(g, "Pirates", Main.resources.getWikiIcons().get(6), rects.get(5));
	    drawLvl(g, "French Revolution", Main.resources.getWikiIcons().get(7), rects.get(6));
	    drawLvl(g, "Carboneria", Main.resources.getWikiIcons().get(8), rects.get(7));
	    drawLvl(g, "Russian Revolution", Main.resources.getWikiIcons().get(9), rects.get(8));
	    drawLvl(g, "World War II", Main.resources.getWikiIcons().get(10), rects.get(9));
	    drawLvl(g, "Vietnam", Main.resources.getWikiIcons().get(11), rects.get(10));
	    drawLvl(g, "Future", Main.resources.getWikiIcons().get(12), rects.get(11));
	}

	public int getID() {
		return id;
	}
	
	public void launchBrowser(String url) {
		try {
			Desktop.getDesktop().browse(URI.create(url));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void drawLvl(Graphics g, String title, Image image, Rectangle rect) {
		g.drawString(title, rect.getX(), rect.getY()-25);
		image.draw(rect.getX(), rect.getY(), 100, 100);
		g.draw(rect);
	}
	
	public void openWikipedia(Rectangle r) {
		if(r.equals(rects.get(0)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Prehistory");
		if(r.equals(rects.get(1)))
			this.launchBrowser("http://en.wikipedia.org/wiki/History_of_Egypt");
		if(r.equals(rects.get(2)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Great_Fire_of_Rome");
		if(r.equals(rects.get(3)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Crusade");
		if(r.equals(rects.get(4)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Conquistadores");
		if(r.equals(rects.get(5)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Pirate");
		if(r.equals(rects.get(6)))
			this.launchBrowser("http://en.wikipedia.org/wiki/French_Revolution");
		if(r.equals(rects.get(7)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Carbonari");
		if(r.equals(rects.get(8)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Russian_Revolution_of_1905");
		if(r.equals(rects.get(9)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Normandy_landings");
		if(r.equals(rects.get(10)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Vietnam_War");
		if(r.equals(rects.get(11)))
			this.launchBrowser("http://en.wikipedia.org/wiki/Special:Random");
	}
}