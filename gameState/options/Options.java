package gameState.options;
import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Options extends BasicGameState {
	
	private int id;
	private Input input;
	
	private Circle soundCircle = null;
	private Rectangle hsRect = null;
	private Rectangle htRect = null;
	private Rectangle boboRect = null;
	private Rectangle aboutRect = null;
	private Rectangle backRect = null;
	
	public Options(int id){
		this.id = id;
	}
	
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {	
		input = container.getInput();
		
		soundCircle = new Circle(1000, 340, 30);
		hsRect = new Rectangle(497, 214, 207, 72);
		htRect = new Rectangle(497, 304, 207, 72);
		boboRect = new Rectangle(497, 394, 207, 72);
		aboutRect = new Rectangle(497, 484, 207, 72);
		backRect = new Rectangle(100, 450, 207, 72);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
	    
	    if(hsRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	            sbg.enterState(Main.highScore);
	        }
	    }
	    if(htRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	            sbg.enterState(Main.howTo);
	        }
	    }
	    if(boboRect.contains(mouseX, container.getHeight()-mouseY)) {
	    	if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	    		Main.resources.getSoundEffects().get(0).play();
	            sbg.enterState(Main.bobopedia);
	        }
	    }
	    if(aboutRect.contains(mouseX, container.getHeight()-mouseY)) {
	    	if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	    		Main.resources.getSoundEffects().get(0).play();
	    		sbg.enterState(Main.about);
	        }
	    }
	    if(backRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	        	sbg.enterState(Main.menu);
	        }
	    }
	    
	    if(soundCircle.contains(mouseX,container.getHeight()-mouseY)) {
		   	if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
		   		if(container.isMusicOn()) {
		   			container.setMusicOn(false);
		   			container.setSoundOn(false);
		   		} else {
		   			container.setMusicOn(true);
		   			container.setSoundOn(true);
		   		}
		   	}
		}
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(0).drawCentered(600, 100);
	    Main.resources.getMenuIcons().get(7).drawCentered(600, 250);
	    Main.resources.getMenuIcons().get(8).drawCentered(600, 340);
	    Main.resources.getMenuIcons().get(9).drawCentered(600, 430);
	    Main.resources.getMenuIcons().get(10).drawCentered(600, 520);
	    Main.resources.getMenuIcons().get(5).draw(100, 450);
	    
	    if(container.isMusicOn())
	    	Main.resources.getMenuIcons().get(11).draw(970, 310, 60, 60);
	    else
	    	Main.resources.getMenuIcons().get(12).draw(970, 310, 60, 60);
	}

	public int getID() {
		return id;
	}
}