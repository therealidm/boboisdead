package gameState.options;
import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class HowTo extends BasicGameState {
	
	private int id;
	private Input input;
	private Rectangle backRect = null;
	
	public HowTo(int id) {
		this.id = id;
	}
	
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		input = container.getInput();
		backRect = new Rectangle(100, 450, 207, 72);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
	    
	    if(backRect.contains(mouseX, container.getHeight()-mouseY)) {
		   	if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
		   		Main.resources.getSoundEffects().get(0).play();
		   		sbg.enterState(Main.options);
		   	}
		}
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
	    Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(5).draw(100, 450);
	    Main.resources.getMenuIcons().get(16).draw(190, 10, 800, 320);
	    Main.resources.getMenuIcons().get(17).draw(420, 340, 370, 260);
	}

	public int getID() {
		return id;
	}
}