package gameState.options;
import java.util.ArrayList;

import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import database.Profile;

public class HighScore extends BasicGameState {
	
	private int id;
	private ArrayList<Profile> profiles = null;
	private Input input;
	
	private Rectangle backRect = null;
	
	public HighScore(int id) {
		this.id = id;
	}

	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		profiles = new ArrayList<Profile>();
		input = container.getInput();
		
		backRect = new Rectangle(100, 450, 207, 72);
	}
	
	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
		
	    if(backRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	        	sbg.enterState(Main.options);
	        }
	    }
	}

	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(0).drawCentered(600, 100);
	    Main.resources.getMenuIcons().get(5).draw(100, 450);

		g.drawString("Position", 350, 200);
		g.drawString("Name", 500, 200);
		g.drawString("Kill", 1000, 200);
		
		if(profiles.size()!=0) {
			for(int i = 0; i<5 && i<profiles.size(); i++) {
				this.drawProfile(g, i, profiles.get(i), 250+i*50);
			}
		} else {
			g.drawString("No profile found.", 350, 250);
		}	
	}
	
	public void enter(GameContainer container, StateBasedGame sbg) {
		profiles = Main.db.rank();
	}

	public int getID() {
		return id;
	}
	
	public void drawProfile(Graphics g, int rank, Profile profile, float y) {
		g.drawString(Integer.toString(rank+1), 350, y);
		g.drawString(profile.getName(), 500, y);
		g.drawString(Integer.toString(profile.getEnemiesKilled()), 1000, y);
	}
}