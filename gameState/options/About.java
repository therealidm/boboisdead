package gameState.options;
import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class About extends BasicGameState {
	
	private int id;
	private Input input;
	private Rectangle backRect = null;
	
	public About(int id) {
		this.id = id;
	}
	
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		input = container.getInput();
		backRect = new Rectangle(100, 450, 207, 72);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
	    
	    if(backRect.contains(mouseX, container.getHeight()-mouseY)) {
		   	if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
		   		Main.resources.getSoundEffects().get(0).play();
		   		sbg.enterState(Main.options);
		   	}
		}
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
	    Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(0).drawCentered(600, 100);
	    Main.resources.getMenuIcons().get(5).draw(100, 450);
	    g.drawString("BOBO IS DEAD v1.0.0", 400, 250);
	    g.drawString("Authors IDM", 400, 270);
	    g.drawString("(c) Copyright IDM", 400, 290);
	    g.drawString("And don't worry, Bobo is NOT dead", 400, 310);
	}

	public int getID() {
		return id;
	}
}