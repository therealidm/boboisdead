package gameState;

import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class Victory extends BasicGameState {

	private int id;
	private Input input;
	
	private Rectangle contRect = null;
	
	public Victory(int id) {
		this.id = id;
	}

	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		input = container.getInput();
		contRect = new Rectangle(893, 450, 207, 72);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
	    
	    if(contRect.contains(mouseX, container.getHeight()-mouseY)) {
	         if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	 Main.resources.getSoundEffects().get(0).play();
	        	 sbg.enterState(Main.levelSelection, new FadeOutTransition(), new FadeInTransition());
	         }
	    }
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {	
		Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(0).drawCentered(600, 100);
	    Main.resources.getMenuIcons().get(6).draw(893, 450);
	    g.drawString("Contratulations! You win!", 400, 250);
	    g.drawString("You killed " + Main.enemiesKilled + " enemies!", 400, 270);
	    g.drawString("You collected " + Main.bonusCollected + " bonuses!", 400, 290);
	}

	public int getID() {
		return id;
	}
	
	public void enter(GameContainer container, StateBasedGame sbg) throws SlickException {
		if(!Main.resources.getSoundtrack().get(0).playing())
			Main.resources.getSoundtrack().get(0).loop();
		
		Main.profile1.unlockLvl(Main.levelSelected);
		Main.profile1.setEnemiesKilled(Main.profile1.getEnemiesKilled() + Main.enemiesKilled);
		Main.db.updateUser(Main.profile1);
		if(Main.multiplayer) {
			Main.profile2.unlockLvl(Main.levelSelected);
			Main.profile2.setEnemiesKilled(Main.profile2.getEnemiesKilled() + Main.enemiesKilled);
			Main.db.updateUser(Main.profile2);
		}
	}
}