package gameState.level;

import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import physics.Physics;
import controller.EnemyController;
import controller.Player1Controller;
import controller.Player2Controller;
import entity.character.Player;

public class LevelState extends BasicGameState{
	
	private int id;
	private float scale =(float) 1.171875;
	private boolean pause = false;
	private Input input;
	
	private Level level;
	private Player player1;
	private Player player2;
	
	private Player1Controller player1Controller;
	private Player2Controller player2Controller;
	private EnemyController enemyController;
	
	@SuppressWarnings("unused")
	private LevelObjectLoader lol;
	private Physics physics = null;
	
	private Circle soundCircle = null;
	private Rectangle exitRect = null;
	private Rectangle contRect = null;
	
	public LevelState(int id) {
		this.id = id;
	}

	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		physics = new Physics();
		input = container.getInput();
		
		soundCircle = new Circle(530, 380, 30);
		contRect = new Rectangle(427, 164, 207, 72);
		exitRect = new Rectangle(427, 254, 207, 72);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		if(!pause) {
			player1Controller.handleInput(container.getInput(), delta);
			if(Main.multiplayer)
				player2Controller.handleInput(container.getInput(), delta);
			enemyController.handleEnemies(delta);
			physics.handlePhysics(level, delta);
		
			if(level.getPlayers().isEmpty()) {
				sbg.enterState(Main.gameOver, new FadeOutTransition(), new FadeInTransition());
			}
			if(level.getEnemies().isEmpty()) {
				sbg.enterState(Main.victory, new FadeOutTransition(), new FadeInTransition());
			}
		} else {
			float mouseX = Mouse.getX()/scale;
			float mouseY = Mouse.getY()/scale;
		    
			if(contRect.contains(mouseX, container.getHeight()-mouseY-88)) {
			   	if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
			   		Main.resources.getSoundEffects().get(0).play();
			   		pause = !pause;
			   	}
			}
			if(exitRect.contains(mouseX, container.getHeight()-mouseY-88)) {
			   	if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
			   		Main.resources.getSoundEffects().get(0).play();
			   		sbg.enterState(Main.levelSelection, new FadeOutTransition(), new FadeInTransition());
			   	}
			}
			
			if(soundCircle.contains(mouseX, container.getHeight()-mouseY-88)) {
			   	if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
			   		if(container.isMusicOn()) {
			   			container.setMusicOn(false);
			   			container.setSoundOn(false);
			   		} else {
			   			container.setMusicOn(true);
			   			container.setSoundOn(true);
			   		}
			   	}
			}
		}
		
		if(input.isKeyPressed(Input.KEY_P) || input.isButtonPressed(2, Input.ANY_CONTROLLER)) {
				pause = !pause;
	    }
	}

	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		g.scale(this.scale, this.scale);
		level.render(g);
		
		if(pause) {
			Main.resources.getBackgrounds().get(0).draw(230, 50, 600, 400);
			Main.resources.getMenuIcons().get(6).drawCentered(530, 200);
		    Main.resources.getMenuIcons().get(4).drawCentered(530, 290);
		    
		    if(container.isMusicOn())
		    	Main.resources.getMenuIcons().get(11).draw(500, 350, 60, 60);
		    else
		    	Main.resources.getMenuIcons().get(12).draw(500, 350, 60, 60);
		}
	}
	
	public void enter(GameContainer container, StateBasedGame sbg) throws SlickException {
		Main.enemiesKilled = 0;
		Main.bonusCollected = 0;
		pause = false;
		
		player1 = new Player(128, 384, Main.profile1.getName());
		player1Controller = new Player1Controller(player1, Main.joystick);
		if(Main.multiplayer) {
			player2 = new Player(228, 384, Main.profile2.getName());
			player2Controller = new Player2Controller(player2);
			
			level = new Level(player1, player2);
		} else {
			level = new Level(player1);
		}
		
		lol = new LevelObjectLoader(level);
		enemyController = new EnemyController(level);
		
		if(!Main.resources.getSoundtrack().get(Main.levelSelected).playing())
			Main.resources.getSoundtrack().get(Main.levelSelected).loop();
	}
	
	public void leave(GameContainer container, StateBasedGame sbg) throws SlickException {
		Main.resources.getSoundtrack().get(Main.levelSelected).stop();
	}

	public int getID() {
		return id;
	}
}