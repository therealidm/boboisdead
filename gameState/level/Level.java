package gameState.level;

import java.util.ArrayList;

import entity.Explosion;
import entity.HUD;
import entity.LevelObject;
import entity.character.Character;
import entity.character.Player;
import entity.character.enemy.Enemy;
import gameState.level.tile.AirTile;
import gameState.level.tile.SolidTile;
import gameState.level.tile.Tile;
import main.Main;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

public class Level {
	
	private TiledMap map;
	private Tile[][] tiles;
	private HUD hud;
	
	private Image background;
	private float scrollXFactor = 0;
	private float scrollYFactor = 0;
	
	private ArrayList<Character> characters;
	private Player player1 = null;
	private Player player2 = null;
	private ArrayList<LevelObject> levelObjects;
	private ArrayList<Explosion> explosions;
	
	private int offsetX;
	private int offsetY;
	
	public Level(Player player1) throws SlickException {
		map = Main.resources.getTiledMaps().get(Main.levelSelected);
		background = Main.resources.getBackgrounds().get(Main.levelSelected);
		this.setBackgroundScroll();
		
		characters = new ArrayList<Character>();
		levelObjects = new ArrayList<LevelObject>();
		explosions = new ArrayList<Explosion>();
		
		this.player1 = player1;
		addCharacter(player1);
		
		hud = new HUD(player1);
		this.loadTileMap();
	}
	
	public Level(Player player1, Player player2) throws SlickException {
		map = Main.resources.getTiledMaps().get(Main.levelSelected);
		background = Main.resources.getBackgrounds().get(Main.levelSelected);
		this.setBackgroundScroll();
		
		characters = new ArrayList<Character>();
		levelObjects = new ArrayList<LevelObject>();
		explosions = new ArrayList<Explosion>();
		
		this.player1 = player1;
		this.player2 = player2;
		addCharacter(player1);
		addCharacter(player2);
		
		hud = new HUD(player1, player2);
		this.loadTileMap();
	}
	
	public void render(Graphics g) {
		offsetX = getXOffset();
        offsetY = getYOffset();
        
        background.draw(offsetX*scrollXFactor, offsetY*scrollYFactor);
        map.render(-(offsetX%32), -(offsetY%32), offsetX/32, offsetY/32, 96, 16);
        hud.render();
		
		for(Character c: characters) {
			c.render(offsetX, offsetY, g);
		}	
		for(LevelObject obj : levelObjects){
            obj.render(offsetX, offsetY, g);
        }
		for(Explosion exp: explosions) {
			exp.render();
		}
	}
	
	public void addCharacter(Character c) {
		this.characters.add(c);
	}
	
	public void loadTileMap() {
		tiles = new Tile[map.getWidth()][map.getHeight()];
		int layerIndex = map.getLayerIndex("CollisionLayer");
		
		if(layerIndex == -1) {
			System.err.println("The layer CollisionLayer doesn't exist.");
			System.exit(0);
		}
		
		for(int x = 0; x<map.getWidth(); x++) {
			for(int y = 0; y<map.getHeight(); y++) {

				int tileID = map.getTileId(x, y, layerIndex);
				Tile tile = null;
				
				if(map.getTileProperty(tileID, "tileType", "solid").equals("air")) {
					tile = new AirTile(x,y);
				} else {
					tile = new SolidTile(x,y);
				}		
				tiles[x][y] = tile;
			}
		}
	}

	public Tile[][] getTiles() {
		return tiles;
	}

	public ArrayList<Character> getCharacters() {
		return characters;
	}
	
	public int getXOffset() {
		Player player;
		if(Main.multiplayer && player1.isDead())
			player = player2;
		else
			player = player1;
		
        int offsetX = 0;
        int halfWidth = (int) (1200/1.171875/2);
        int maxX = (int) (map.getWidth()*32)-halfWidth;
 
        if(player.getX() < halfWidth) {
            offsetX = 0;
        } else if(player.getX() > maxX) {
            offsetX = maxX-halfWidth;
        } else {
            offsetX = (int) (player.getX()-halfWidth);
        }
        return offsetX;
    }
	
	public int getYOffset() {
		Player player;
		if(Main.multiplayer && player1.isDead())
			player = player2;
		else
			player = player1;
		
        int offsetY = 0;
        int halfHeigth = (int) (600/1.171875/2);
        int maxY = (int) (map.getHeight()*32)-halfHeigth;
 
        if(player.getY() < halfHeigth) {
            offsetY = 0;
        } else if(player.getY() > maxY) {
            offsetY = maxY-halfHeigth;
        } else {
            offsetY = (int) (player.getY()-halfHeigth);
        }
        return offsetY;
    }
	
	public void removeObject(LevelObject obj) {
        levelObjects.remove(obj);
    }
 
    public void removeObjects(ArrayList<LevelObject> objects) {
        levelObjects.removeAll(objects);
    }
    
    public void addLevelObject(LevelObject obj) {
        levelObjects.add(obj);
    }
    
    public ArrayList<LevelObject> getLevelObjects() {
        return levelObjects;
    }
    
   private void setBackgroundScroll() { 
    	float backgroundXScrollValue = (float) (background.getWidth()-1200/1.171825);//l'illegalit� pi� totale nella scala
        float backgroundYScrollValue = (float) (background.getHeight()-600/1.171825);
 
        float mapXScrollValue = (float) (map.getWidth()*32-1200/1.171825);
        float mapYScrollValue = (float) (map.getHeight()*32-600/1.171825);
 
        scrollXFactor = backgroundXScrollValue/mapXScrollValue * -1;
        scrollYFactor = backgroundYScrollValue/mapYScrollValue * -1;
    }
   
   public boolean isOnScreen(LevelObject lO) {
		if(lO.getX() < (offsetX+32*32))
			return true;
		else
			return false;
	}

   public Player getPlayer1() {
	   return player1;
   }
   
   public Player getPlayer2() {
	   return player2;
   }
   
   public ArrayList<Player> getPlayers() {
	   ArrayList<Player> players = new ArrayList<Player>();
	   for(Character c: characters)
		   if(c instanceof Player)
			   players.add((Player) c);
	   return players;
   }
   
   public ArrayList<Character> getEnemies() {
	   ArrayList<Character> enemies = new ArrayList<Character>();
	   for(Character c: characters)
		   if(c instanceof Enemy)
			   enemies.add(c);
	   return enemies;
   }
   
   public void removeCharacters(ArrayList<Character> deadCharacters) {
	   for(Character c: deadCharacters)
		   explosions.add(new Explosion(c.getX()-offsetX, c.getY()-offsetY));
       characters.removeAll(deadCharacters);
   }
   
   public ArrayList<Explosion> getExplosions() {
	   return explosions;
   }
   
   public void removeExplosions(ArrayList<Explosion> exp) {
       explosions.removeAll(exp);
   }
}