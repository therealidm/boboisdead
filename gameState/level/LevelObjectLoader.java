package gameState.level;

import org.newdawn.slick.SlickException;

import entity.bonus.IncreaseDamage;
import entity.bonus.IncreaseHealth;
import entity.character.enemy.BasicEnemy;
import entity.character.enemy.BossEnemy;
import main.Main;

public class LevelObjectLoader {
	
	public LevelObjectLoader(Level level) throws SlickException {
	
		switch(Main.levelSelected) {
		
		case 1: level.addCharacter(new BasicEnemy(1280, 384));
				level.addCharacter(new BasicEnemy(1888, 96));
				level.addCharacter(new BasicEnemy(3360, 224));
				level.addCharacter(new BasicEnemy(4992, 384));
				level.addCharacter(new BasicEnemy(6144, 96));
				level.addCharacter(new BasicEnemy(7296, 384));
				
				level.addCharacter(new BossEnemy(9952, 256));
				
				level.addLevelObject(new IncreaseDamage(2176, 224));
		        level.addLevelObject(new IncreaseDamage(7808, 64));
		        
		        level.addLevelObject(new IncreaseHealth(34, 416));
		        level.addLevelObject(new IncreaseHealth(8704, 384));
		        break;
		        
		case 2: level.addCharacter(new BasicEnemy(1280, 384));
				level.addCharacter(new BasicEnemy(1888, 96));
				level.addCharacter(new BasicEnemy(2784, 384));
				level.addCharacter(new BasicEnemy(3360, 224));
				level.addCharacter(new BasicEnemy(4992, 384));
				level.addCharacter(new BasicEnemy(6144, 96));
				level.addCharacter(new BasicEnemy(7296, 384));
		
				level.addCharacter(new BossEnemy(9952, 256));
		
				level.addLevelObject(new IncreaseDamage(2176, 224));
				level.addLevelObject(new IncreaseDamage(7808, 64));
        
				level.addLevelObject(new IncreaseHealth(34, 416));
				level.addLevelObject(new IncreaseHealth(8704, 384));
				break;
			
		case 3: level.addCharacter(new BasicEnemy(1280, 384));
				level.addCharacter(new BasicEnemy(1888, 96));
				level.addCharacter(new BasicEnemy(2784, 384));
				level.addCharacter(new BasicEnemy(3360, 224));
				level.addCharacter(new BasicEnemy(4992, 384));
				level.addCharacter(new BasicEnemy(6144, 96));
				level.addCharacter(new BasicEnemy(7008, 384));
				level.addCharacter(new BasicEnemy(7296, 384));

				level.addCharacter(new BossEnemy(9952, 256));

				level.addLevelObject(new IncreaseDamage(2176, 224));
				level.addLevelObject(new IncreaseDamage(3456, 416));
				level.addLevelObject(new IncreaseDamage(7808, 64));

				level.addLevelObject(new IncreaseHealth(34, 416));
				level.addLevelObject(new IncreaseHealth(3552, 416));
				level.addLevelObject(new IncreaseHealth(8704, 384));
				break;
		
		case 4: level.addCharacter(new BasicEnemy(1280, 384));
				level.addCharacter(new BasicEnemy(1888, 96));
				level.addCharacter(new BasicEnemy(2784, 384));
				level.addCharacter(new BasicEnemy(3360, 224));
				level.addCharacter(new BasicEnemy(4992, 384));
				level.addCharacter(new BasicEnemy(6144, 96));
				level.addCharacter(new BasicEnemy(7008, 384));
				level.addCharacter(new BasicEnemy(7296, 384));
				level.addCharacter(new BasicEnemy(8416, 160));

				level.addCharacter(new BossEnemy(9952, 256));

				level.addLevelObject(new IncreaseDamage(2176, 224));
				level.addLevelObject(new IncreaseDamage(3456, 416));
				level.addLevelObject(new IncreaseDamage(7808, 64));

				level.addLevelObject(new IncreaseHealth(34, 416));
				level.addLevelObject(new IncreaseHealth(3552, 416));
				level.addLevelObject(new IncreaseHealth(8704, 384));
				break;
				
		case 5: level.addCharacter(new BasicEnemy(1280, 384));
				level.addCharacter(new BasicEnemy(1888, 96));
				level.addCharacter(new BasicEnemy(2784, 384));
				level.addCharacter(new BasicEnemy(3360, 224));
				level.addCharacter(new BasicEnemy(4992, 384));
				level.addCharacter(new BasicEnemy(5472, 384));
				level.addCharacter(new BasicEnemy(6144, 96));
				level.addCharacter(new BasicEnemy(7008, 384));
				level.addCharacter(new BasicEnemy(7296, 384));
				level.addCharacter(new BasicEnemy(8416, 160));

				level.addCharacter(new BossEnemy(9952, 256));

				level.addLevelObject(new IncreaseDamage(2176, 224));
				level.addLevelObject(new IncreaseDamage(3456, 416));
				level.addLevelObject(new IncreaseDamage(7808, 64));

				level.addLevelObject(new IncreaseHealth(34, 416));
				level.addLevelObject(new IncreaseHealth(3552, 416));
				level.addLevelObject(new IncreaseHealth(7200, 384));
				level.addLevelObject(new IncreaseHealth(8704, 384));
				break;
		
		case 6: level.addCharacter(new BasicEnemy(1280, 384));
				level.addCharacter(new BasicEnemy(1792, 64));
				level.addCharacter(new BasicEnemy(1888, 96));
				level.addCharacter(new BasicEnemy(2784, 384));
				level.addCharacter(new BasicEnemy(3360, 224));
				level.addCharacter(new BasicEnemy(4992, 384));
				level.addCharacter(new BasicEnemy(5472, 384));
				level.addCharacter(new BasicEnemy(6144, 96));
				level.addCharacter(new BasicEnemy(7008, 384));
				level.addCharacter(new BasicEnemy(7296, 384));
				level.addCharacter(new BasicEnemy(8416, 160));

				level.addCharacter(new BossEnemy(9952, 256));

				level.addLevelObject(new IncreaseDamage(2176, 224));
				level.addLevelObject(new IncreaseDamage(3456, 416));
				level.addLevelObject(new IncreaseDamage(7808, 64));

				level.addLevelObject(new IncreaseHealth(34, 416));
				level.addLevelObject(new IncreaseHealth(3552, 416));
				level.addLevelObject(new IncreaseHealth(7200, 384));
				level.addLevelObject(new IncreaseHealth(8704, 384));
				break;
		
		case 7: level.addCharacter(new BasicEnemy(1280, 384));
				level.addCharacter(new BasicEnemy(1792, 64));
				level.addCharacter(new BasicEnemy(1888, 96));
				level.addCharacter(new BasicEnemy(2784, 384));
				level.addCharacter(new BasicEnemy(3360, 224));
				level.addCharacter(new BasicEnemy(4416, 384));
				level.addCharacter(new BasicEnemy(4992, 384));
				level.addCharacter(new BasicEnemy(5472, 384));
				level.addCharacter(new BasicEnemy(6144, 96));
				level.addCharacter(new BasicEnemy(7008, 384));
				level.addCharacter(new BasicEnemy(7296, 384));
				level.addCharacter(new BasicEnemy(8416, 160));
		
				level.addCharacter(new BossEnemy(9952, 256));
		
				level.addLevelObject(new IncreaseDamage(2176, 224));
				level.addLevelObject(new IncreaseDamage(3456, 416));
				level.addLevelObject(new IncreaseDamage(7808, 64));
		
				level.addLevelObject(new IncreaseHealth(34, 416));
				level.addLevelObject(new IncreaseHealth(3552, 416));
				level.addLevelObject(new IncreaseHealth(7200, 384));
				level.addLevelObject(new IncreaseHealth(8704, 384));
				break;
		
		case 8: level.addCharacter(new BasicEnemy(1280, 384));
				level.addCharacter(new BasicEnemy(1792, 64));
				level.addCharacter(new BasicEnemy(1888, 96));
				level.addCharacter(new BasicEnemy(2784, 384));
				level.addCharacter(new BasicEnemy(3360, 224));
				level.addCharacter(new BasicEnemy(3904, 256));
				level.addCharacter(new BasicEnemy(4416, 384));
				level.addCharacter(new BasicEnemy(4992, 384));
				level.addCharacter(new BasicEnemy(5472, 384));
				level.addCharacter(new BasicEnemy(6144, 96));
				level.addCharacter(new BasicEnemy(7008, 384));
				level.addCharacter(new BasicEnemy(7296, 384));
				level.addCharacter(new BasicEnemy(8416, 160));
		
				level.addCharacter(new BossEnemy(9952, 256));
		
				level.addLevelObject(new IncreaseDamage(2176, 224));
				level.addLevelObject(new IncreaseDamage(3456, 416));
				level.addLevelObject(new IncreaseDamage(7808, 64));
		
				level.addLevelObject(new IncreaseHealth(34, 416));
				level.addLevelObject(new IncreaseHealth(3552, 416));
				level.addLevelObject(new IncreaseHealth(7200, 384));
				level.addLevelObject(new IncreaseHealth(8704, 384));
				break;
		
		case 9: level.addCharacter(new BasicEnemy(1280, 384));
				level.addCharacter(new BasicEnemy(1792, 64));
				level.addCharacter(new BasicEnemy(1888, 96));
				level.addCharacter(new BasicEnemy(2784, 384));
				level.addCharacter(new BasicEnemy(3360, 224));
				level.addCharacter(new BasicEnemy(3904, 256));
				level.addCharacter(new BasicEnemy(4416, 384));
				level.addCharacter(new BasicEnemy(4992, 384));
				level.addCharacter(new BasicEnemy(5472, 384));
				level.addCharacter(new BasicEnemy(6144, 96));
				level.addCharacter(new BasicEnemy(6752, 256));
				level.addCharacter(new BasicEnemy(7008, 384));
				level.addCharacter(new BasicEnemy(7296, 384));
				level.addCharacter(new BasicEnemy(8416, 160));
		
				level.addCharacter(new BossEnemy(9952, 256));
		
				level.addLevelObject(new IncreaseDamage(2176, 224));
				level.addLevelObject(new IncreaseDamage(3456, 416));
				level.addLevelObject(new IncreaseDamage(7808, 64));
		
				level.addLevelObject(new IncreaseHealth(34, 416));
				level.addLevelObject(new IncreaseHealth(3552, 416));
				level.addLevelObject(new IncreaseHealth(7200, 384));
				level.addLevelObject(new IncreaseHealth(8704, 384));
				break;
		
		case 10: level.addCharacter(new BasicEnemy(1280, 384));
				 level.addCharacter(new BasicEnemy(1792, 64));
				 level.addCharacter(new BasicEnemy(1888, 96));
				 level.addCharacter(new BasicEnemy(2784, 384));
				 level.addCharacter(new BasicEnemy(3360, 224));
				 level.addCharacter(new BasicEnemy(3904, 256));
				 level.addCharacter(new BasicEnemy(4416, 384));
				 level.addCharacter(new BasicEnemy(4992, 384));
				 level.addCharacter(new BasicEnemy(5472, 384));
				 level.addCharacter(new BasicEnemy(6144, 96));
				 level.addCharacter(new BasicEnemy(6656, 160));
				 level.addCharacter(new BasicEnemy(6752, 256));
				 level.addCharacter(new BasicEnemy(7008, 384));
				 level.addCharacter(new BasicEnemy(7296, 384));
				 level.addCharacter(new BasicEnemy(8416, 160));
		
				 level.addCharacter(new BossEnemy(9952, 256));
		
				 level.addLevelObject(new IncreaseDamage(2176, 224));
				 level.addLevelObject(new IncreaseDamage(1760, 64));
				 level.addLevelObject(new IncreaseDamage(3456, 416));
				 level.addLevelObject(new IncreaseDamage(7808, 64));
		
				 level.addLevelObject(new IncreaseHealth(34, 416));
				 level.addLevelObject(new IncreaseHealth(2560, 384));
				 level.addLevelObject(new IncreaseHealth(3552, 416));
				 level.addLevelObject(new IncreaseHealth(7200, 384));
				 level.addLevelObject(new IncreaseHealth(8704, 384));
				 break;
				
		case 11: level.addCharacter(new BasicEnemy(1280, 384));
				 level.addCharacter(new BasicEnemy(1792, 64));
				 level.addCharacter(new BasicEnemy(1888, 96));
				 level.addCharacter(new BasicEnemy(2784, 384));
				 level.addCharacter(new BasicEnemy(3360, 224));
				 level.addCharacter(new BasicEnemy(3904, 256));
				 level.addCharacter(new BasicEnemy(4416, 384));
				 level.addCharacter(new BasicEnemy(4992, 384));
				 level.addCharacter(new BasicEnemy(5472, 384));
				 level.addCharacter(new BasicEnemy(5920, 160));
				 level.addCharacter(new BasicEnemy(6144, 96));
				 level.addCharacter(new BasicEnemy(6656, 160));
				 level.addCharacter(new BasicEnemy(6752, 256));
				 level.addCharacter(new BasicEnemy(7008, 384));
				 level.addCharacter(new BasicEnemy(7296, 384));
				 level.addCharacter(new BasicEnemy(8416, 160));
		
				 level.addCharacter(new BossEnemy(9952, 256));
		
				 level.addLevelObject(new IncreaseDamage(2176, 224));
				 level.addLevelObject(new IncreaseDamage(1760, 64));
				 level.addLevelObject(new IncreaseDamage(3456, 416));
				 level.addLevelObject(new IncreaseDamage(7808, 64));
		
				 level.addLevelObject(new IncreaseHealth(34, 416));
				 level.addLevelObject(new IncreaseHealth(2560, 384));
				 level.addLevelObject(new IncreaseHealth(3552, 416));
				 level.addLevelObject(new IncreaseHealth(7200, 384));
				 level.addLevelObject(new IncreaseHealth(8704, 384));
				 break;
		
		case 12: level.addCharacter(new BasicEnemy(1280, 384));
				 level.addCharacter(new BasicEnemy(1792, 64));
				 level.addCharacter(new BasicEnemy(1888, 96));
				 level.addCharacter(new BasicEnemy(2784, 384));
				 level.addCharacter(new BasicEnemy(3360, 224));
				 level.addCharacter(new BasicEnemy(3904, 256));
				 level.addCharacter(new BasicEnemy(4256, 384));
				 level.addCharacter(new BasicEnemy(4300, 384));
				 level.addCharacter(new BasicEnemy(4416, 384));
				 level.addCharacter(new BasicEnemy(4992, 384));
				 level.addCharacter(new BasicEnemy(5040, 384));
				 level.addCharacter(new BasicEnemy(5100, 384));
				 level.addCharacter(new BasicEnemy(5200, 384));
				 level.addCharacter(new BasicEnemy(5300, 384));
				 level.addCharacter(new BasicEnemy(5400, 384));
				 level.addCharacter(new BasicEnemy(5472, 384));
				 level.addCharacter(new BasicEnemy(5920, 160));
				 level.addCharacter(new BasicEnemy(6144, 96));
				 level.addCharacter(new BasicEnemy(6656, 160));
				 level.addCharacter(new BasicEnemy(6752, 256));
				 level.addCharacter(new BasicEnemy(6800, 256));
				 level.addCharacter(new BasicEnemy(6900, 256));
				 level.addCharacter(new BasicEnemy(6950, 256));
				 level.addCharacter(new BasicEnemy(7008, 384));
				 level.addCharacter(new BasicEnemy(7296, 384));
				 level.addCharacter(new BasicEnemy(7520, 160));
				 level.addCharacter(new BasicEnemy(8416, 160));
		
				 level.addCharacter(new BossEnemy(9952, 256));
		
				 level.addLevelObject(new IncreaseDamage(1056, 96));
				 level.addLevelObject(new IncreaseDamage(2176, 224));
				 level.addLevelObject(new IncreaseDamage(1760, 64));
				 level.addLevelObject(new IncreaseDamage(3456, 416));
				 level.addLevelObject(new IncreaseDamage(7808, 64));
		
				 level.addLevelObject(new IncreaseHealth(34, 416));
				 level.addLevelObject(new IncreaseHealth(2560, 384));
				 level.addLevelObject(new IncreaseHealth(3552, 416));
				 level.addLevelObject(new IncreaseHealth(7200, 384));
				 level.addLevelObject(new IncreaseHealth(8704, 384));
				 level.addLevelObject(new IncreaseHealth(10112, 384));
				 break;
		}
	}
}