package gameState.profile;
import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import database.Profile;

public class OnePlayer extends BasicGameState {
	
	private int id;
	private TextField textField;
	private int playerOk;
	private Input input;
	
	private Rectangle backRect = null;
	private Rectangle contRect = null;
	
	public OnePlayer(int id){
		this.id = id;
	}
	
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
	    textField = new TextField(container, container.getDefaultFont(), 400, 250, 400, 50);
	    input = container.getInput();
	    
	    backRect = new Rectangle(100, 450, 207, 72);
	    contRect = new Rectangle(893, 450, 207, 72);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		textField.setFocus(true);
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
	    
	    if(backRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	        	sbg.enterState(Main.menu);
	        }
	    }
	    if(contRect.contains(mouseX, container.getHeight()-mouseY)) {
	    	if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON) && (playerOk == 1 || playerOk == 2)) {
	    		Main.resources.getSoundEffects().get(0).play();
	    		sbg.enterState(Main.levelSelection);
	        }
	    }
	    
	    if(input.isKeyPressed(Input.KEY_ENTER)) {
	    	Main.profile1 = new Profile(textField.getText());
	    	if(Main.db.existsUser(textField.getText()))
	    		playerOk = 1;
	    	else
	    		playerOk = 2;
	   }
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(0).drawCentered(600, 100);
	    Main.resources.getMenuIcons().get(5).draw(100, 450);
	    Main.resources.getMenuIcons().get(6).draw(893, 450);
	    g.drawString("Insert your name here and press ENTER:", 400, 220);
	    textField.render(container, g);
	    
	    if(playerOk == 1)
	    	g.drawString("Click CONTINUE to proceed.", 400, 350);
	    if(playerOk == 2)
	    	g.drawString("This profile doesn't exist, a new instance will be created.", 400, 350);
	}

	public int getID() {
		return id;
	}
	
	public void enter(GameContainer container, StateBasedGame sbg) {   
	    playerOk = 0;
	    textField.setText("");
	    Main.multiplayer = false;
    }
	
	public void leave(GameContainer container, StateBasedGame sbg) {
		if(playerOk == 1)
			Main.profile1 = Main.db.loadUser(textField.getText());
		else
			Main.db.newUser(textField.getText());
	}
}