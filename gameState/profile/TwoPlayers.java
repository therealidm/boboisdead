package gameState.profile;
import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import database.Profile;

public class TwoPlayers extends BasicGameState {
	
	private int id;
	private TextField textField1;
	private TextField textField2;
	private int focus;
	private int player1Ok;
	private int player2Ok;
	private Input input;
	
	private Rectangle backRect = null;
	private Rectangle contRect = null;
	
	public TwoPlayers(int id){
		this.id = id;
	}
	
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
	    textField1 = new TextField(container, container.getDefaultFont(), 150, 250, 400, 50);
	    textField2 = new TextField(container, container.getDefaultFont(), 650, 250, 400, 50);
	    input = container.getInput();
	    
	    backRect = new Rectangle(100, 450, 207, 72);
	    contRect = new Rectangle(893, 450, 207, 72);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		if(focus == 1) {
			textField1.setFocus(true);
			textField2.setFocus(false);
		} else {
			textField2.setFocus(true);
			textField1.setFocus(false);
		}
		
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();

	    if(backRect.contains(mouseX, container.getHeight()-mouseY)) {
	         if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	 Main.resources.getSoundEffects().get(0).play();
	        	 sbg.enterState(Main.menu);
	         }
	    }
	    if(contRect.contains(mouseX, container.getHeight()-mouseY)) {
	         if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON) && (player1Ok == 1 || player1Ok == 2)) {
	        	 Main.resources.getSoundEffects().get(0).play();
	        	 if(player2Ok == 0)
	        		 focus = 2;
	        	 if(player2Ok == 1 || player2Ok == 2)
	        		 sbg.enterState(Main.levelSelection);
	         }
	    }
	   
	    if(input.isKeyPressed(Input.KEY_ENTER)) {
	    	if(focus == 1) {
	    		Main.profile1 = new Profile(textField1.getText());
	    		if(Main.db.existsUser(textField1.getText()))
	    			player1Ok = 1;
	    		else
	    			player1Ok = 2;
	    	} else {
	    		Main.profile2 = new Profile(textField2.getText());
	    		if(Main.db.existsUser(textField2.getText()))
	    			player2Ok = 1;
	    		else
	    			player2Ok = 2;
	    	}
	    }
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(0).drawCentered(600, 100);
	    Main.resources.getMenuIcons().get(5).draw(100, 450);
	    Main.resources.getMenuIcons().get(6).draw(893, 450);
	    g.drawString("Insert your names here and press ENTER:", 450, 220);
	    textField1.render(container, g);
	    textField2.render(container, g);
	    
	    if(player1Ok == 1)
	    	g.drawString("Click CONTINUE to proceed.", 150, 350);
	    if(player1Ok == 2)
	    	g.drawString("This profile doesn't exist, a new instance will be created.", 100, 350);
	    if(player2Ok == 1)
	    	g.drawString("Click CONTINUE to proceed.", 650, 350);
	    if(player2Ok == 2)
	    	g.drawString("This profile doesn't exist, a new instance will be created.", 600, 350);
	}

	public int getID() {
		return id;
	}
	
	public void enter(GameContainer container, StateBasedGame sbg) {   
	    player1Ok = 0;
	    player2Ok = 0;
	    textField1.setText("");
	    textField2.setText("");
	    focus = 1;
	    Main.multiplayer = true;
    }
	
	public void leave(GameContainer container, StateBasedGame sbg) {
		if(player1Ok == 1)
			Main.profile1 = Main.db.loadUser(textField1.getText());
		else
			Main.db.newUser(textField1.getText());
		
		if(player2Ok == 1)
			Main.profile2 = Main.db.loadUser(textField2.getText());
		else
			Main.db.newUser(textField2.getText());
	}
}