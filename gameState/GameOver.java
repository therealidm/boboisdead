package gameState;

import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class GameOver extends BasicGameState {
	
	private int id;
	private Input input;
	
	private Rectangle retryRect = null;
	private Rectangle contRect = null;
	
	public GameOver(int id) {
		this.id = id;
	}

	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		input = container.getInput();
		
		retryRect = new Rectangle(100, 450, 207, 72);
		contRect = new Rectangle(893, 450, 207, 72);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
	    
	    if(retryRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	        	sbg.enterState(Main.levelState, new FadeOutTransition(), new FadeInTransition());
	        }
	    }
	    if(contRect.contains(mouseX, container.getHeight()-mouseY)) {
	         if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	 Main.resources.getSoundEffects().get(0).play();
	        	 sbg.enterState(Main.levelSelection, new FadeOutTransition(), new FadeInTransition());
	         }
	    }
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {	
		Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(0).drawCentered(600, 100);
	    Main.resources.getMenuIcons().get(13).draw(100, 450);
	    Main.resources.getMenuIcons().get(6).draw(893, 450);
	    g.drawString("Sorry! You lose!", 400, 250);
	    g.drawString("You killed " + Main.enemiesKilled + " enemies but it wasn't enough.", 400, 270);
	    g.drawString("You collected " + Main.bonusCollected + " bonuses.", 400, 290);
	}

	public int getID() {
		return id;
	}
	
	public void enter(GameContainer container, StateBasedGame sbg) throws SlickException {
		if(!Main.resources.getSoundtrack().get(0).playing())
			Main.resources.getSoundtrack().get(0).loop();
	}
	
	public void leave(GameContainer container, StateBasedGame sbg) {
		Main.resources.getSoundtrack().get(0).stop();
	}
}