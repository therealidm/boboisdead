package gameState;
import main.Main;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Menu extends BasicGameState {
	
	private int id;
	private Input input;
	
	private Rectangle onePRect = null;
	private Rectangle twoPRect = null;
	private Rectangle optionsRect = null;
	private Rectangle exitRect = null;
	
	public Menu(int id) {
		this.id = id;
	}
	
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		input = container.getInput();
		
		onePRect = new Rectangle(497, 214, 207, 72);
		twoPRect = new Rectangle(497, 304, 207, 72);
		optionsRect = new Rectangle(497, 394, 207, 72);
		exitRect = new Rectangle(497, 484, 207, 72);
	}

	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		int mouseX = Mouse.getX();
	    int mouseY = Mouse.getY();
	    
	    if(onePRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	        	sbg.enterState(Main.onePlayer);
	        }
	    }
	    if(twoPRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	        	sbg.enterState(Main.twoPlayers);
	        }
	    }
	    if(optionsRect.contains(mouseX, container.getHeight()-mouseY)) {
	        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
	        	Main.resources.getSoundEffects().get(0).play();
	        	sbg.enterState(Main.options);
	        }
	    }
	    if(exitRect.contains(mouseX, container.getHeight()-mouseY)) {
    		if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
    			Main.resources.getSoundEffects().get(0).play();
    			System.exit(0);
	        }
	    }
	}
	
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		g.setColor(Color.red);
		
	    Main.resources.getBackgrounds().get(0).draw(0, 0, 1200, 600);
	    Main.resources.getMenuIcons().get(0).drawCentered(600, 100);
	    Main.resources.getMenuIcons().get(1).drawCentered(600, 250);
	    Main.resources.getMenuIcons().get(2).drawCentered(600, 340);
	    Main.resources.getMenuIcons().get(3).drawCentered(600, 430);
	    Main.resources.getMenuIcons().get(4).drawCentered(600, 520);
	}

	public int getID() {
		return id;
	}
	
	public void enter(GameContainer container, StateBasedGame sbg) throws SlickException {
		if(!Main.resources.getSoundtrack().get(0).playing())
			Main.resources.getSoundtrack().get(0).loop();
	}
}