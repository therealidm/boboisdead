package physics;

import java.util.ArrayList;

import main.Main;
import gameState.level.Level;
import gameState.level.tile.Tile;
import entity.Explosion;
import entity.LevelObject;
import entity.Shot;
import entity.bonus.Bonus;
import entity.bonus.IncreaseDamage;
import entity.bonus.IncreaseHealth;
import entity.character.Character;
import entity.character.Player;
import entity.character.enemy.BasicEnemy;
import entity.character.enemy.BossEnemy;
import entity.character.enemy.Enemy;

public class Physics {
	
	private final float gravity = 0.0015f;
	
	public void handlePhysics(Level level, int delta) {
		this.handleCharacters(level, delta);
		handleLevelObjects(level, delta);
		handleExplosions(level, delta);
	}
	
	private void handleExplosions(Level level, int delta) {
		ArrayList<Explosion> expiredExplosions = new ArrayList<Explosion>();
		
		for(Explosion exp: level.getExplosions()) {
			exp.update(delta);
			if(exp.shouldRemove())
				expiredExplosions.add(exp);
		}
		level.removeExplosions(expiredExplosions);
	}

	public void handleCharacters(Level level, int delta) {
		ArrayList<Character> deadCharacters = new ArrayList<Character>();
		
		for(Character c: level.getCharacters()) {
			if(!c.isMoving())
				c.decelerate(delta);
			
			this.handleGameObject(c,level,delta);
			this.handleShots(c, level, delta);
			
			if(c instanceof Player) {
	            ArrayList<LevelObject> removeQueue = new ArrayList<LevelObject>();

	            for(LevelObject obj : level.getLevelObjects()) {
	                if(obj instanceof Bonus) {
	                    if(obj.getBoundingShape().checkCollision(c.getBoundingShape())) {
	                        removeQueue.add(obj);
	                        
	                        Main.bonusCollected++;
	                        if(obj instanceof IncreaseHealth)
	                        	c.setMaximumHealth(c.getMaximumHealth()*2);
	                        if(obj instanceof IncreaseDamage)
	                        	c.setDamage(c.getDamage()*2);
	                    }
	                }
	            }
	            level.removeObjects(removeQueue);
	        }
			if(c.isDead()) {
				deadCharacters.add(c);
				if(c instanceof BasicEnemy || c instanceof BossEnemy)
					Main.enemiesKilled++;
			}
		}
		level.removeCharacters(deadCharacters);
	}
	
	private void handleGameObject(LevelObject obj, Level level, int delta) {
		obj.setOnGround(isOnGround(obj, level.getTiles()));
		
		if((!obj.isOnGround() || obj.getYVelocity()<0) && !(obj instanceof Shot))
			obj.applyGravity(gravity*delta);
		else
			obj.setYVelocity(0);

		float xMovement = obj.getXVelocity()*delta;
		float yMovement = obj.getYVelocity()*delta;
		
		float stepX = 0;
		float stepY = 0;
		
		if(xMovement != 0) {
			stepY = Math.abs(yMovement)/Math.abs(xMovement);
			
			if(yMovement < 0)
				stepY = -stepY;
			
			if(xMovement > 0)
				stepX = 1;
			else
				stepX = -1;
			
			if((stepY > 1 || stepY < -1) && stepY != 0) {
				stepX = Math.abs(stepX)/Math.abs(stepY);
				
				if(xMovement < 0)
					stepX = -stepX;
				
				if(yMovement < 0)
					stepY = -1;
				else
					stepY = 1;
			}
		} else 
			if(yMovement != 0) {
				if(yMovement > 0)
					stepY = 1;
				else
					stepY = -1;
		}
		
		while(xMovement != 0 || yMovement != 0) {
			
			if(xMovement != 0){
				
				if((xMovement > 0 && xMovement < stepX) || (xMovement > stepX && xMovement < 0)) {
					stepX = xMovement;
					xMovement = 0;
				} else
					xMovement -=stepX; 
				
				obj.setX(obj.getX()+stepX);
				
				if(checkCollision(obj,level.getTiles())) {
					if(obj instanceof Shot)
						((Shot) obj).setRemove(true);
					if(obj instanceof Enemy)
						((Enemy) obj).setVsWall(true);
					obj.setX(obj.getX()-stepX);
					obj.setXVelocity(0);
					xMovement = 0;
				} else {
					if(obj instanceof Enemy)
						((Enemy) obj).setVsWall(false);
				}
			}
			
			if(yMovement != 0){
				
				if((yMovement > 0 && yMovement < stepY) || (yMovement > stepY && yMovement < 0)) {
					stepY = yMovement;
					yMovement = 0;
				} else
					yMovement -=stepY; 
				
				obj.setY(obj.getY()+stepY);
				
				if(checkCollision(obj,level.getTiles())) {
					obj.setY(obj.getY()-stepY);
					obj.setYVelocity(0);
					yMovement = 0;
				}
			}
		}
	}
	
	public boolean checkCollision(LevelObject obj, Tile[][] mapTiles) {
		
		ArrayList<Tile> tiles = obj.getBoundingShape().getTilesOccupying(mapTiles);
		for(Tile t: tiles){
			if(t.getBoundingShape() != null) {
				if(t.getBoundingShape().checkCollision(obj.getBoundingShape())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean isOnGround(LevelObject obj, Tile[][] mapTiles) {
		
		ArrayList<Tile> tiles = obj.getBoundingShape().getGroundTiles(mapTiles);
		
		obj.getBoundingShape().movePosition(0, 1);
		
		for(Tile t: tiles) {
			if(t.getBoundingShape() != null) {
				if(t.getBoundingShape().checkCollision(obj.getBoundingShape())) {
					obj.getBoundingShape().movePosition(0,-1);
					return true;
				}
			}
		}
		obj.getBoundingShape().movePosition(0,-1);		
		return false;
	}
	
	private void handleLevelObjects(Level level, int delta) {
        for(LevelObject obj : level.getLevelObjects()) {
            handleGameObject(obj,level,delta);
        }
    }
	
	private void handleShots(Character c, Level level, int delta) {
			if(c instanceof Player)
				handlePlayerShots((Player) c, level, delta);
			if(c instanceof Enemy)
				handleEnemyShots((Enemy) c, level, delta);
	}
	
	private void handlePlayerShots(Player p, Level level, int delta) {
		ArrayList<Shot> removeQueue = new ArrayList<Shot>();
		
		for(Shot s: p.getShots()) {
			this.handleGameObject(s, level, delta);
			
				for(Character enemy : level.getCharacters()) {
					if(enemy instanceof Enemy) {
						if(s.getBoundingShape().checkCollision(enemy.getBoundingShape())) {
							enemy.hit(s.getDamage());
							s.setRemove(true);
						}
					}
				}
			if(s.shouldRemove())
				removeQueue.add(s);
		}
		p.removeShots(removeQueue);
	}
	
	private void handleEnemyShots(Enemy e, Level level, int delta) {
		ArrayList<Shot> removeQueue = new ArrayList<Shot>();
		
		for(Shot s: e.getShots()) {
			this.handleGameObject(s, level, delta);
			
				for(Character player : level.getCharacters()) {
					if(player instanceof Player) {
						if(s.getBoundingShape().checkCollision(player.getBoundingShape())) {
							player.hit(s.getDamage());
							s.setRemove(true);
						}
					}
				}
			if(s.shouldRemove())
				removeQueue.add(s);
		}
		e.removeShots(removeQueue);	
	}
}