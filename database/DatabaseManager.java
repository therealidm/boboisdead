package database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DatabaseManager {
	Connection connection = null;
	Statement statement = null;
	
	public DatabaseManager() {
		
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:users.db");
			System.out.println("Got connection.");
			statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS Users ("
					+ "Name STRING UNIQUE,"
					+ "Level1 INTEGER DEFAULT 1,"
					+ "Level2 INTEGER DEFAULT 0,"
					+ "Level3 INTEGER DEFAULT 0,"
					+ "Level4 INTEGER DEFAULT 0,"
					+ "Level5 INTEGER DEFAULT 0,"
					+ "Level6 INTEGER DEFAULT 0,"
					+ "Level7 INTEGER DEFAULT 0,"
					+ "Level8 INTEGER DEFAULT 0,"
					+ "Level9 INTEGER DEFAULT 0,"
					+ "Level10 INTEGER DEFAULT 0,"
					+ "Level11 INTEGER DEFAULT 0,"
					+ "Level12 INTEGER DEFAULT 0,"
					+ "EnemiesKilled INTEGER DEFAULT 0,"
					+ "PRIMARY KEY(Name));");
			System.out.println("Table created.");
			
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public boolean newUser(String name) {
		try {
			
			if(this.existsUser(name)) {
				System.out.println(name + " istance already exists.");
				return false;
			} else {
				statement.executeUpdate("INSERT INTO Users (Name) VALUES ('" + name + "')");
				System.out.println(name + " istance is created.");
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean existsUser(String name) {
		ResultSet resultSet = null;
		try {
			resultSet = statement.executeQuery("SELECT COUNT(*) FROM Users WHERE Name='" + name + "'");
			
			if(resultSet.getInt(1) == 1) {
				return true;
			} else {
				return false;	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public Profile loadUser(String name) {
		Profile profile = new Profile(name);
		
		try {
			if(this.existsUser(name)) {
				profile.setLvl1(this.retLvl1(name));
				profile.setLvl2(this.retLvl2(name));
				profile.setLvl3(this.retLvl3(name));
				profile.setLvl4(this.retLvl4(name));
				profile.setLvl5(this.retLvl5(name));
				profile.setLvl6(this.retLvl6(name));
				profile.setLvl7(this.retLvl7(name));
				profile.setLvl8(this.retLvl8(name));
				profile.setLvl9(this.retLvl9(name));
				profile.setLvl10(this.retLvl10(name));
				profile.setLvl11(this.retLvl11(name));
				profile.setLvl12(this.retLvl12(name));
				profile.setEnemiesKilled(this.retEnemiesKilled(name));
				
				return profile;
			}else{
				System.out.println("Can't load data: the user doesn't exist.");
				return null;
			}		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return profile;
	}
	
	public void updateUser(Profile profile) {
		
		try {
			if(this.existsUser(profile.getName())) {
				statement.executeUpdate("UPDATE Users SET Level2=" + boolToInt(profile.getLvl2()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level3=" + boolToInt(profile.getLvl3()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level4=" + boolToInt(profile.getLvl4()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level5=" + boolToInt(profile.getLvl5()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level6=" + boolToInt(profile.getLvl6()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level7=" + boolToInt(profile.getLvl7()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level8=" + boolToInt(profile.getLvl8()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level9=" + boolToInt(profile.getLvl9()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level10=" + boolToInt(profile.getLvl10()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level11=" + boolToInt(profile.getLvl11()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET Level12=" + boolToInt(profile.getLvl12()) + " WHERE Name='" + profile.getName() + "'");
				statement.executeUpdate("UPDATE Users SET EnemiesKilled=" + profile.getEnemiesKilled() + " WHERE Name='" + profile.getName() + "'");
			} else {
				System.out.println("Can't update database: the user doesn't exist.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet findUser(String name) {
		ResultSet resultSet = null;
		try {
			resultSet = statement.executeQuery("SELECT * FROM Users WHERE Name='" + name + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultSet;
	}
	
	public ArrayList<Profile> rank() {
		ResultSet resultSet = null;
		ArrayList<Profile> profiles = new ArrayList<Profile>();
		Profile profile = new Profile(null);
		try {
			resultSet = statement.executeQuery("SELECT * FROM users ORDER BY EnemiesKilled DESC");
			while(resultSet.next()) {
				profile = new Profile(resultSet.getString("Name"));
				profile.setLvl1(resultSet.getInt("Level1")!=0);
				profile.setLvl2(resultSet.getInt("Level2")!=0);
				profile.setLvl3(resultSet.getInt("Level3")!=0);
				profile.setLvl4(resultSet.getInt("Level4")!=0);
				profile.setLvl5(resultSet.getInt("Level5")!=0);
				profile.setLvl6(resultSet.getInt("Level6")!=0);
				profile.setLvl7(resultSet.getInt("Level7")!=0);
				profile.setLvl8(resultSet.getInt("Level8")!=0);
				profile.setLvl9(resultSet.getInt("Level9")!=0);
				profile.setLvl10(resultSet.getInt("Level10")!=0);
				profile.setLvl11(resultSet.getInt("Level11")!=0);
				profile.setLvl12(resultSet.getInt("Level12")!=0);
				profile.setEnemiesKilled(resultSet.getInt("EnemiesKilled"));
				profiles.add(profile);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return profiles;
	}
	
	public boolean retLvl1(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level1")!=0);
	}
	
	public boolean retLvl2(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level2")!=0);
	}
	
	public boolean retLvl3(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level3")!=0);
	}
	
	public boolean retLvl4(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level4")!=0);
	}
	
	public boolean retLvl5(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level5")!=0);
	}
	
	public boolean retLvl6(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level6")!=0);
	}
	
	public boolean retLvl7(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level7")!=0);
	}
	
	public boolean retLvl8(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level8")!=0);
	}
	
	public boolean retLvl9(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level9")!=0);
	}
	
	public boolean retLvl10(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level10")!=0);
	}
	
	public boolean retLvl11(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level11")!=0);
	}
	
	public boolean retLvl12(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return (resultSet.getInt("Level12")!=0);
	}
	
	public int retEnemiesKilled(String name) throws SQLException {
		ResultSet resultSet = null;
		resultSet = this.findUser(name);
		return resultSet.getInt("EnemiesKilled");
	}
	
	public int boolToInt(boolean b) {
		return b?1:0;
	}
}