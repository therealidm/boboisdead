package database;

public class Profile {
	private String name = null;
	private boolean lvl1;
	private boolean lvl2;
	private boolean lvl3;
	private boolean lvl4;
	private boolean lvl5;
	private boolean lvl6;
	private boolean lvl7;
	private boolean lvl8;
	private boolean lvl9;
	private boolean lvl10;
	private boolean lvl11;
	private boolean lvl12;
	private int enemiesKilled;
	
	public Profile(String name) {
		this.name = name;
		this.lvl1 = true;
		this.lvl2 = false;
		this.lvl3 = false;
		this.lvl4 = false;
		this.lvl5 = false;
		this.lvl6 = false;
		this.lvl7 = false;
		this.lvl8 = false;
		this.lvl9 = false;
		this.lvl10 = false;
		this.lvl11 = false;
		this.lvl12 = false;
		this.enemiesKilled = 0;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean getLvl1() {
		return lvl1;
	}
	
	public void setLvl1(boolean lvl1) {
		this.lvl1 = lvl1;
	}
	
	public boolean getLvl2() {
		return lvl2;
	}
	
	public void setLvl2(boolean lvl2) {
		this.lvl2 = lvl2;
	}
	
	public boolean getLvl3() {
		return lvl3;
	}
	
	public void setLvl3(boolean lvl3) {
		this.lvl3 = lvl3;
	}
	
	public boolean getLvl4() {
		return lvl4;
	}
	
	public void setLvl4(boolean lvl4) {
		this.lvl4 = lvl4;
	}
	
	public boolean getLvl5() {
		return lvl5;
	}
	
	public void setLvl5(boolean lvl5) {
		this.lvl5 = lvl5;
	}
	
	public boolean getLvl6() {
		return lvl6;
	}
	
	public void setLvl6(boolean lvl6) {
		this.lvl6 = lvl6;
	}
	
	public boolean getLvl7() {
		return lvl7;
	}
	
	public void setLvl7(boolean lvl7) {
		this.lvl7 = lvl7;
	}
	
	public boolean getLvl8() {
		return lvl8;
	}
	
	public void setLvl8(boolean lvl8) {
		this.lvl8 = lvl8;
	}
	
	public boolean getLvl9() {
		return lvl9;
	}
	
	public void setLvl9(boolean lvl9) {
		this.lvl9 = lvl9;
	}
	
	public boolean getLvl10() {
		return lvl10;
	}
	
	public void setLvl10(boolean lvl10) {
		this.lvl10 = lvl10;
	}
	
	public boolean getLvl11() {
		return lvl11;
	}
	
	public void setLvl11(boolean lvl11) {
		this.lvl11 = lvl11;
	}
	
	public boolean getLvl12() {
		return lvl12;
	}
	
	public void setLvl12(boolean lvl12) {
		this.lvl12 = lvl12;
	}
	
	public int getEnemiesKilled() {
		return enemiesKilled;
	}
	
	public void setEnemiesKilled(int enemiesKilled) {
		this.enemiesKilled = enemiesKilled;
	}
	
	public String toString() {
		return "Name=" + this.getName()
			   + " Level1 =" + this.getLvl1()
			   + " Level2 =" + this.getLvl2()
			   + " Level3 =" + this.getLvl3()
			   + " Level4 =" + this.getLvl4()
			   + " Level5 =" + this.getLvl5()
			   + " Level6 =" + this.getLvl6()
			   + " Level7 =" + this.getLvl7()
			   + " Level8 =" + this.getLvl8()
			   + " Level9 =" + this.getLvl9()
			   + " Level10 =" + this.getLvl10()
			   + " Level11 =" + this.getLvl11()
			   + " Level12 =" + this.getLvl12()
			   + " Enemies Killed=" + this.getEnemiesKilled();
	}
	
	public void unlockLvl(int i) {
		switch(i) {
		case 1:
			setLvl2(true);
			break;
		case 2:
			setLvl3(true);
			break;
		case 3:
			setLvl4(true);
			break;
		case 4:
			setLvl5(true);
			break;
		case 5:
			setLvl6(true);
			break;
		case 6:
			setLvl7(true);
			break;
		case 7:
			setLvl8(true);
			break;
		case 8:
			setLvl9(true);
			break;
		case 9:
			setLvl10(true);
			break;
		case 10:
			setLvl11(true);
			break;
		case 11:
			setLvl12(true);
			break;
		}
	}
}