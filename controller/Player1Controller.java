package controller;

import org.newdawn.slick.Input;

import entity.character.Player;

public class Player1Controller extends PlayerController {
	
	public Player1Controller (Player player) {
		super(player);
		up = Input.KEY_W;
		left = Input.KEY_A;
		right = Input.KEY_D;
		shoot = Input.KEY_SPACE;
		joystickNumber = Input.ANY_CONTROLLER;
	}
	
	public Player1Controller (Player player, boolean joystick) {
		super(player, joystick);
		up = Input.KEY_W;
		left = Input.KEY_A;
		right = Input.KEY_D;
		shoot = Input.KEY_SPACE;
		joystickNumber = Input.ANY_CONTROLLER;
	}
}