package controller;

import org.newdawn.slick.Input;

import entity.character.Player;

public class Player2Controller extends PlayerController {
	
	public Player2Controller (Player player) {
		super(player);
		up = Input.KEY_UP;
		left = Input.KEY_LEFT;
		right = Input.KEY_RIGHT;
		shoot = Input.KEY_0;
		joystickNumber = Input.ANY_CONTROLLER;
	}
	
	public Player2Controller (Player player, boolean joystick) {
		super(player, joystick);
		up = Input.KEY_UP;
		left = Input.KEY_LEFT;
		right = Input.KEY_RIGHT;
		shoot = Input.KEY_0;
		joystickNumber = Input.ANY_CONTROLLER;
	}
}