package controller;

import org.newdawn.slick.Input;

import entity.character.Player;

public class PlayerController {
	
	protected int up;
	protected int left;
	protected int right;
	protected int shoot;
	protected int joystickNumber;
	
	private boolean keyboard = true;
	private boolean joystick = false;
	private long startTime = 0;
	
	protected Player player = null;
	
	public PlayerController (Player player) {
		this.player = player;
	}
	
	public PlayerController (Player player, boolean joystick) {
		this.player = player;
		this.keyboard = !joystick;
		this.joystick = joystick;
	}
	
	public void handleInput(Input i, int delta) {
		if(keyboard) {
			handleKeyboardInput(i, delta);
		} else {
			if(joystick) {
				handleJoystickInput(i, delta);
			}
		}
	}
	
	public void handleKeyboardInput(Input i, int delta) {
		
		if(i.isKeyDown(left)) {
			player.moveLeft(delta);
		} else 
			if(i.isKeyDown(right)) {
			player.moveRight(delta);
		} else {
			player.setMoving(false);
		}
		
		if(i.isKeyDown(up) && player.getYVelocity() == 0)
			player.jump();
		
		if(i.isKeyDown(shoot) && (System.currentTimeMillis()-startTime > 250)) {
			startTime = System.currentTimeMillis();
			player.shoot(delta);
		}
	}

	public void handleJoystickInput(Input i, int delta) {
		
		if(i.isControllerLeft(joystickNumber)) {
			player.moveLeft(delta);
		} else 
			if(i.isControllerRight(joystickNumber)) {
			player.moveRight(delta);
		} else {
			player.setMoving(false);
		}
		
		if(i.isButton1Pressed(joystickNumber) && player.getYVelocity() == 0)
			player.jump();
		
		if(i.isButton3Pressed(joystickNumber) && (System.currentTimeMillis()-startTime > 250)) {
			startTime = System.currentTimeMillis();
			player.shoot(delta);
		}
	}
}