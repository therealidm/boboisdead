package controller;

import java.util.ArrayList;

import gameState.level.Level;
import entity.character.Character;
import entity.character.Player;
import entity.character.enemy.Enemy;

public class EnemyController {
	
	private Level level;
	private long startTime = 0;
	
	public EnemyController(Level level) {
		this.level = level;
	}
	
	public void handleEnemies(int delta) {
		for(Character e: level.getCharacters()) {
			if(level.isOnScreen(e) && (e instanceof Enemy))
				updatePosition(e, delta, nearestPlayer(e, level.getPlayers()));
		}
	}
	
	private Player nearestPlayer(Character e, ArrayList<Player> players) {
		double distance = Math.hypot(players.get(0).getX()-e.getX(), players.get(0).getY()-e.getY());
		Player nearestPlayer = players.get(0);
		
		for(Player p: players)
			if(distance > Math.hypot(p.getX()-e.getX(), p.getY()-e.getY())) {
				distance = Math.hypot(p.getX()-e.getX(), p.getY()-e.getY());
				nearestPlayer = p;
			}
		return nearestPlayer;
	}

	public void updatePosition(Character enemy, int delta, Player player) {
		if(player.getX() < enemy.getX())
			enemy.moveLeft(delta);
		else
			enemy.moveRight(delta);
		
		if(player.getY() < enemy.getY() || ((Enemy) enemy).isVsWall())
			enemy.jump();
		
		if((enemy.getY() < player.getY() + 64) && (enemy.getY() > player.getY() - 64) && (System.currentTimeMillis()-startTime > 1000)) {
			startTime = System.currentTimeMillis();
			enemy.shoot(delta);
		}
	}
}