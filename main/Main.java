package main;
import gameState.GameOver;
import gameState.Menu;
import gameState.Storyline;
import gameState.Victory;
import gameState.level.LevelState;
import gameState.levelSelection.LevelSelection;
import gameState.options.About;
import gameState.options.Bobopedia;
import gameState.options.HighScore;
import gameState.options.HowTo;
import gameState.options.Options;
import gameState.profile.TwoPlayers;
import gameState.profile.OnePlayer;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import database.DatabaseManager;
import database.Profile;

public class Main extends StateBasedGame {
	
	public static final int menu = 0;
	public static final int onePlayer = 1;
	public static final int levelSelection = 2;
	public static final int twoPlayers = 3;
	public static final int options = 4;
	public static final int about = 5;
	public static final int highScore = 6;
	public static final int bobopedia = 7;
	public static final int howTo = 8;
	public static final int storyline = 9;
	public static final int levelState = 10;
	public static final int victory = 11;
	public static final int gameOver = 12;
	
	public static int levelSelected = 0;
	public static int enemiesKilled = 0;
	public static int bonusCollected = 0;
	public static boolean multiplayer = false;
	public static boolean joystick = false;
	
	public static DatabaseManager db = new DatabaseManager();
	public static Resources resources = null;
	public static int levelsCount = 12;
	public static Profile profile1 = new Profile(null);
	public static Profile profile2 = new Profile(null);

	public Main(String name) {
		super(name);
	}

	public void initStatesList(GameContainer container) throws SlickException {	
		resources = new Resources();
		this.addState(new Menu(menu));
		this.addState(new OnePlayer(onePlayer));
		this.addState(new LevelSelection(levelSelection));
		this.addState(new TwoPlayers(twoPlayers));
		this.addState(new Options(options));
		this.addState(new About(about));
		this.addState(new HighScore(highScore));
		this.addState(new Bobopedia(bobopedia));
		this.addState(new LevelState(levelState));
		this.addState(new Victory(victory));
		this.addState(new GameOver(gameOver));
		this.addState(new HowTo(howTo));
		this.addState(new Storyline(storyline));
		enterState(menu);
	}
	
	public static void main(String[] args) throws SlickException {
		AppGameContainer app = new AppGameContainer(new Main("BOBO IS DEAD"));
		app.setDisplayMode(1200, 600, false);
		app.setAlwaysRender(true);
		app.setShowFPS(false);
		app.start();
	   }
}