package main;
import java.io.File;
import java.util.ArrayList;

import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.tiled.TiledMap;

public class Resources {
	
	private ArrayList<Music> soundtrack = null;
	private ArrayList<Sound> soundEffects = null;
	private ArrayList<Image> backgrounds = null;
	private ArrayList<Image> levelIcons = null;
	private ArrayList<Image> menuIcons = null;
	private ArrayList<Image> wikiIcons = null;
	private ArrayList<TiledMap> tiledMaps = null;
	private ArrayList<SpriteSheet> playerMove = null;
	private ArrayList<Image> playerStatic = null;
	private ArrayList<SpriteSheet> basicEnemies = null;
	private ArrayList<SpriteSheet> bossEnemies = null;
	private ArrayList<Image> shots = null;
	private ArrayList<File> xmlFiles = null;
	private ArrayList<Image> huds = null;
	private ArrayList<String> storyFiles = null;
	
	private Image particle = null;
	private SpriteSheet bonusHealth = null;
	private SpriteSheet bonusDamage = null;
	
	public Resources() {
		
		try {
		soundtrack = new ArrayList<Music>();
		soundtrack.add(new Music("data/audio/soundtrack/backgroundMusic.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level1.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level2.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level3.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level4.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level5.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level6.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level7.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level8.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level9.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level10.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level11.ogg"));
		soundtrack.add(new Music("data/audio/soundtrack/level12.ogg"));
		
		soundEffects = new ArrayList<Sound>();
		soundEffects.add(new Sound("data/audio/soundEffects/waterDrop.ogg"));
		soundEffects.add(new Sound("data/audio/soundEffects/shot.ogg"));
		soundEffects.add(new Sound("data/audio/soundEffects/hit.ogg"));
		soundEffects.add(new Sound("data/audio/soundEffects/death.ogg"));
		
		backgrounds = new ArrayList<Image>();
		backgrounds.add(new Image("data/video/backgrounds/background.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level1.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level2.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level3.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level4.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level5.jpeg"));
		backgrounds.add(new Image("data/video/backgrounds/level6.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level7.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level8.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level9.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level10.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level11.jpg"));
		backgrounds.add(new Image("data/video/backgrounds/level12.jpg"));
		
		levelIcons = new ArrayList<Image>();
		levelIcons.add(new Image("data/video/levelIcons/lock.png"));
		levelIcons.add(new Image("data/video/levelIcons/level1.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level2.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level3.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level4.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level5.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level6.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level7.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level8.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level9.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level10.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level11.jpg"));
		levelIcons.add(new Image("data/video/levelIcons/level12.jpg"));
		
		menuIcons = new ArrayList<Image>();
		menuIcons.add(new Image("data/video/menuIcons/title.png"));
		menuIcons.add(new Image("data/video/menuIcons/1player.png"));
		menuIcons.add(new Image("data/video/menuIcons/2players.png"));
		menuIcons.add(new Image("data/video/menuIcons/options.png"));
		menuIcons.add(new Image("data/video/menuIcons/exit.png"));
		menuIcons.add(new Image("data/video/menuIcons/back.png"));
		menuIcons.add(new Image("data/video/menuIcons/continue.png"));
		menuIcons.add(new Image("data/video/menuIcons/highscores.png"));
		menuIcons.add(new Image("data/video/menuIcons/howto.png"));
		menuIcons.add(new Image("data/video/menuIcons/bobopedia.png"));
		menuIcons.add(new Image("data/video/menuIcons/about.png"));
		menuIcons.add(new Image("data/video/menuIcons/soundOn.png"));
		menuIcons.add(new Image("data/video/menuIcons/soundOff.png"));
		menuIcons.add(new Image("data/video/menuIcons/retry.png"));
		menuIcons.add(new Image("data/video/menuIcons/joystickYes.png"));
		menuIcons.add(new Image("data/video/menuIcons/joystickNo.png"));
		menuIcons.add(new Image("data/video/menuIcons/keyboard.PNG"));
		menuIcons.add(new Image("data/video/menuIcons/360.jpeg"));
		
		wikiIcons = new ArrayList<Image>();
		wikiIcons.add(null);
		wikiIcons.add(new Image("data/video/wikiIcons/level1.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level2.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level3.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level4.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level5.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level6.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level7.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level8.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level9.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level10.jpg"));
		wikiIcons.add(new Image("data/video/wikiIcons/level11.JPEG"));
		wikiIcons.add(new Image("data/video/wikiIcons/level12.jpg"));
		
		tiledMaps = new ArrayList<TiledMap>();
		tiledMaps.add(null);
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level1.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level2.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level3.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level4.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level5.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level6.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level7.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level8.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level9.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level10.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level11.tmx"));
		tiledMaps.add(new TiledMap("data/video/tiledMaps/level12.tmx"));
		
		playerMove = new ArrayList<SpriteSheet>();
		playerMove.add(null);
		playerMove.add(new SpriteSheet("data/video/playerMove/level1.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level2.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level3.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level4.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level5.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level6.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level7.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level8.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level9.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level10.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level11.png", 64, 64));
		playerMove.add(new SpriteSheet("data/video/playerMove/level12.png", 64, 64));
		
		playerStatic = new ArrayList<Image>();
		playerStatic.add(null);
		playerStatic.add(new Image("data/video/playerStatic/level1.png"));
		playerStatic.add(new Image("data/video/playerStatic/level2.png"));
		playerStatic.add(new Image("data/video/playerStatic/level3.png"));
		playerStatic.add(new Image("data/video/playerStatic/level4.png"));
		playerStatic.add(new Image("data/video/playerStatic/level5.png"));
		playerStatic.add(new Image("data/video/playerStatic/level6.png"));
		playerStatic.add(new Image("data/video/playerStatic/level7.png"));
		playerStatic.add(new Image("data/video/playerStatic/level8.png"));
		playerStatic.add(new Image("data/video/playerStatic/level9.png"));
		playerStatic.add(new Image("data/video/playerStatic/level10.png"));
		playerStatic.add(new Image("data/video/playerStatic/level11.png"));
		playerStatic.add(new Image("data/video/playerStatic/level12.png"));
		
		bonusHealth = new SpriteSheet("data/video/bonuses/medikit.png", 32, 32);
		bonusDamage = new SpriteSheet("data/video/bonuses/turret.png", 32, 32);
		
		basicEnemies = new ArrayList<SpriteSheet>();
		basicEnemies.add(null);
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level1.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level2.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level3.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level4.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level5.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level6.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level7.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level8.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level9.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level10.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level11.png", 64, 64));
		basicEnemies.add(new SpriteSheet("data/video/basicEnemies/level12.png", 64, 64));
		
		shots = new ArrayList<Image>();
		shots.add(null);
		shots.add(new Image("data/video/shots/level1.png"));
		shots.add(new Image("data/video/shots/level2.png"));
		shots.add(new Image("data/video/shots/level3.png"));
		shots.add(new Image("data/video/shots/level4.png"));
		shots.add(new Image("data/video/shots/level5.png"));
		shots.add(new Image("data/video/shots/level6.png"));
		shots.add(new Image("data/video/shots/level7.png"));
		shots.add(new Image("data/video/shots/level8.png"));
		shots.add(new Image("data/video/shots/level9.png"));
		shots.add(new Image("data/video/shots/level10.png"));
		shots.add(new Image("data/video/shots/level11.png"));
		shots.add(new Image("data/video/shots/level12.png"));
		
		particle = new Image("data/miscellaneous/particle.png");
		xmlFiles = new ArrayList<File>();
		xmlFiles.add(null);
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter1.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter2.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter3.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter4.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter5.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter6.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter7.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter8.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter9.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter10.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter11.xml"));
		xmlFiles.add(new File("data/miscellaneous/xmlFiles/particleEmitter12.xml"));
		
		huds = new ArrayList<Image>();
		huds.add(new Image("data/video/huds/hud100.png"));
		huds.add(new Image("data/video/huds/hud80.png"));
		huds.add(new Image("data/video/huds/hud60.png"));
		huds.add(new Image("data/video/huds/hud40.png"));
		huds.add(new Image("data/video/huds/hud20.png"));
		huds.add(new Image("data/video/huds/hud0.png"));
		
		bossEnemies = new ArrayList<SpriteSheet>();
		bossEnemies.add(null);
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level1.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level2.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level3.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level4.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level5.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level6.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level7.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level8.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level9.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level10.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level11.png", 128, 128));
		bossEnemies.add(new SpriteSheet("data/video/bossEnemies/level12.png", 128, 128));
		
		storyFiles = new ArrayList<String>();
		storyFiles.add(null);
		storyFiles.add(new String("data/miscellaneous/storyFiles/level1.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level2.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level3.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level4.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level5.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level6.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level7.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level8.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level9.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level10.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level11.txt"));
		storyFiles.add(new String("data/miscellaneous/storyFiles/level12.txt"));
		
		} catch(SlickException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<SpriteSheet> getPlayerMove() {
		return playerMove;
	}

	public ArrayList<Image> getPlayerStatic() {
		return playerStatic;
	}

	public ArrayList<Sound> getSoundEffects() {
		return soundEffects;
	}

	public ArrayList<Image> getBackgrounds() {
		return backgrounds;
	}

	public ArrayList<Image> getLevelIcons() {
		return levelIcons;
	}

	public ArrayList<Image> getMenuIcons() {
		return menuIcons;
	}

	public ArrayList<Image> getWikiIcons() {
		return wikiIcons;
	}

	public ArrayList<TiledMap> getTiledMaps() {
		return tiledMaps;
	}

	public ArrayList<Music> getSoundtrack() {
		return soundtrack;
	}
	
	public SpriteSheet getBonusHealth() {
		return bonusHealth;
	}
	
	public SpriteSheet getBonusDamage() {
		return bonusDamage;
	}
	
	public ArrayList<SpriteSheet> getBasicEnemies() {
		return basicEnemies;
	}
	
	public ArrayList<SpriteSheet> getBossEnemies() {
		return bossEnemies;
	}
	
	public ArrayList<Image> getShots() {
		return shots;
	}
	
	public ArrayList<File> getXmlFiles() {
		return xmlFiles;
	}
	
	public Image getParticle() {
		return particle;
	}
	
	public ArrayList<Image> getHuds() {
		return huds;
	}
	
	public ArrayList<String> getStoryFiles() {
		return storyFiles;
	}
}