package entity;

import org.newdawn.slick.Graphics;

import physics.AABoundingRect;
import physics.BoundingShape;

public abstract class LevelObject {
	protected float x;
	protected float y;
	protected BoundingShape boundingShape;
	
	protected float xVelocity = 0;
	protected float yVelocity = 0;
	protected float maximumFallSpeed = 1;
	
	protected boolean onGround = true;
	
	public LevelObject(float x, float y) {
		this.x = x;
		this.y = y;
		
		boundingShape = new AABoundingRect(x, y, 32, 32);
	}

	public void applyGravity(float gravity) {
		if(yVelocity < maximumFallSpeed) {
			yVelocity = yVelocity + gravity;
			
			if(yVelocity > maximumFallSpeed) {
				yVelocity = maximumFallSpeed;
			}
		}
	}
	
	public void updateBoundingShape() {
		boundingShape.updatePosition(x, y);
	}
	
	public boolean isOnGround() {
		return onGround;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
		this.updateBoundingShape();
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
		this.updateBoundingShape();
	}

	public float getXVelocity() {
		return xVelocity;
	}

	public void setXVelocity(float xVelocity) {
		this.xVelocity = xVelocity;
	}

	public float getYVelocity() {
		return yVelocity;
	}

	public void setYVelocity(float yVelocity) {
		this.yVelocity = yVelocity;
	}
	
	public void setOnGround(boolean b) {
		this.onGround = b;
	}
	
	public BoundingShape getBoundingShape() {
		return this.boundingShape;
	}
	
	public abstract void render(float offsetX, float offsetY, Graphics g);
}