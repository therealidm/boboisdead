package entity.character;

import main.Main;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import entity.Shot;
import physics.AABoundingRect;

public class Player extends Character {
	
	private String name;

	public Player(float x, float y, String name) throws SlickException {
		super(x, y);
		this.name = name;
		setSprite(Main.resources.getPlayerStatic().get(Main.levelSelected));
		setMovingAnimation(Main.resources.getPlayerMove().get(Main.levelSelected), 125, 64);
		boundingShape = new AABoundingRect(x, y, 64, 64);
		
		health = maxHealth = 100;
		damage = 25;
		
		accelerationSpeed = 0.001f;
        maximumSpeed = 0.15f;
        maximumFallSpeed = 0.3f;
        decelerationSpeed = 0.001f;
	}
	
	public void render(float offsetX, float offsetY, Graphics g) {
		if(movingAnimations != null && moving){
            movingAnimations.get(facing).draw(x-offsetX, y-offsetY);             
        } else {            
        	sprites.get(facing).draw(x-offsetX, y-offsetY);       
        }
		
		g.drawString(name, x-offsetX, y-offsetY-20);
		
		for(Shot s: shots) {
			s.render(offsetX, offsetY, g);
		}
	}
}