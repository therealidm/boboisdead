package entity.character.enemy;

import main.Main;

import org.newdawn.slick.SlickException;

import physics.AABoundingRect;

public class BasicEnemy extends Enemy {

	public BasicEnemy(float x, float y) throws SlickException {
		super(x, y);
		setSprite(Main.resources.getBasicEnemies().get(Main.levelSelected).getSprite(0, 0));
		setMovingAnimation(Main.resources.getBasicEnemies().get(Main.levelSelected), 125, 64);
		boundingShape = new AABoundingRect(x, y, 64, 64);
		
		health = maxHealth = 100 + ((Main.levelSelected-1) * 10);
		damage = 10 + ((Main.levelSelected-1) * 2);
		
		accelerationSpeed = 0.001f;
        maximumSpeed = 0.12f;
        maximumFallSpeed = 0.3f;
        decelerationSpeed = 0.001f;
	}
}