package entity.character.enemy;

import main.Main;

import org.newdawn.slick.SlickException;

import entity.Shot;
import enums.Facing;
import physics.AABoundingRect;

public class BossEnemy extends Enemy {

	public BossEnemy(float x, float y) throws SlickException {
		super(x, y);
		setSprite(Main.resources.getBossEnemies().get(Main.levelSelected).getSprite(0, 0));
		setMovingAnimation(Main.resources.getBossEnemies().get(Main.levelSelected), 125, 128);
		boundingShape = new AABoundingRect(x, y, 128, 128);
		
		health = maxHealth = 1500 + ((Main.levelSelected-2) * 500);
		damage = 30 + ((Main.levelSelected-2) * 20);
		
		accelerationSpeed = 0.002f;
        maximumSpeed = 0.15f;
        maximumFallSpeed = 0.3f;
        decelerationSpeed = 0.001f;
	}
	
	public void shoot(int delta) {
		Shot s = new Shot(this.x+64, this.y+32, this.damage);
		shots.add(s);
		if(facing == Facing.LEFT)
			s.shootLeft(delta);
		else
			s.shootRight(delta);
		
		s = new Shot(this.x+64, this.y+64, this.damage);
		shots.add(s);
		if(facing == Facing.LEFT)
			s.shootLeft(delta);
		else
			s.shootRight(delta);
		
		s = new Shot(this.x+64, this.y+96, this.damage);
		shots.add(s);
		if(facing == Facing.LEFT)
			s.shootLeft(delta);
		else
			s.shootRight(delta);
	}
}