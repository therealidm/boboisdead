package entity.character.enemy;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import entity.Shot;
import entity.character.Character;
import enums.Facing;

public class Enemy extends Character {
	
	private boolean vsWall = false;

	public Enemy(float x, float y) throws SlickException {
		super(x, y);
		facing = Facing.LEFT;
	}
	
	public boolean isVsWall() {
		return vsWall;
	}

	public void setVsWall(boolean vsWall) {
		this.vsWall = vsWall;
	}
	
	public void render(float offsetX, float offsetY, Graphics g) {
		if(movingAnimations != null && moving){
            movingAnimations.get(facing).draw(x-offsetX, y-offsetY);             
        } else {            
        	sprites.get(facing).draw(x-offsetX, y-offsetY);       
        }
		
		g.drawString(health + "/" + maxHealth, x-offsetX, y-offsetY-20);
		
		for(Shot s: shots) {
			s.render(offsetX, offsetY, g);
		}
	}
}