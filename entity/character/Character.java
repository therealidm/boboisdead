package entity.character;

import java.util.ArrayList;
import java.util.HashMap;

import main.Main;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import entity.LevelObject;
import entity.Shot;
import enums.Facing;

public abstract class Character extends LevelObject {

	protected Facing facing;
	protected HashMap<Facing,Image> sprites;
	protected HashMap<Facing,Animation> movingAnimations;
    
    protected boolean moving = false;
    protected float accelerationSpeed = 1;
    protected float decelerationSpeed = 1;
    protected float maximumSpeed = 1;
    
    protected int health;
	protected int maxHealth;
	protected boolean dead = false;
	protected int damage = 0;
	protected ArrayList<Shot> shots = new ArrayList<Shot>();
	
	public Character(float x, float y) throws SlickException {
		super(x, y);
		facing = Facing.RIGHT;
	}
	
	public void render(float offsetX, float offsetY, Graphics g) {
		if(movingAnimations != null && moving){
            movingAnimations.get(facing).draw(x-offsetX, y-offsetY);             
        } else {            
        	sprites.get(facing).draw(x-offsetX, y-offsetY);       
        }
		
		for(Shot s: shots) {
			s.render(offsetX, offsetY, g);
		}
	}
	
	protected void setSprite(Image i){
        sprites = new HashMap<Facing,Image>();
        sprites.put(Facing.RIGHT, i);
        sprites.put(Facing.LEFT, i.getFlippedCopy(true, false));
    }
	
	protected void setMovingAnimation(SpriteSheet spriteSheet, int frameDuration, int frameDimension) {
        movingAnimations = new HashMap<Facing,Animation>();
        movingAnimations.put(Facing.RIGHT, new Animation(spriteSheet, frameDuration));
        movingAnimations.put(Facing.LEFT, new Animation(new SpriteSheet(spriteSheet.getFlippedCopy(true, false), frameDimension, frameDimension), frameDuration));
    }
	
	public boolean isMoving() {
        return moving;
    }
	
	public void setMoving(boolean b) {
        moving = b;
    }
	
	public void decelerate(int delta) {
        if(xVelocity > 0) {
            xVelocity -= decelerationSpeed * delta;
            if(xVelocity < 0)
                xVelocity = 0;
        } else if(xVelocity < 0) {
            xVelocity += decelerationSpeed * delta;
            if(xVelocity > 0)
                xVelocity = 0;
        }
    }
	
	public void jump() {
        if(onGround)
            yVelocity = -0.6f;
    }
	
	public void moveLeft(int delta) {
        if(xVelocity > -maximumSpeed) {
            xVelocity -= accelerationSpeed*delta;
            if(xVelocity < -maximumSpeed) {
                xVelocity = -maximumSpeed;
            }
        }
        moving = true;
        facing = Facing.LEFT;
    }
	
	public void moveRight(int delta) {
        if(xVelocity < maximumSpeed) {
            xVelocity += accelerationSpeed*delta;
            if(xVelocity > maximumSpeed) {
                xVelocity = maximumSpeed;
            }
        }
        moving = true;
        facing = Facing.RIGHT;
    }
	
	public int hit(int damage) {
		Main.resources.getSoundEffects().get(2).play();
		health -= damage;
		if(health <= 0) {
			Main.resources.getSoundEffects().get(3).play();
			dead = true;
			return 0;
		}
		return health;
	}
	
	public boolean isDead() {
		return dead;
	}
	
	public int getDamage() {
		return damage;
	}

	public void setDamage(int newDamage) {
		damage = newDamage;
	}
	
	public int getHealth() {
		return health;
	}
	
	public int getMaximumHealth() {
		return maxHealth;
	}
	
	public void setMaximumHealth(int maximumHealth) {
		this.maxHealth = maximumHealth;
		health = maxHealth;
	}
	
	public void shoot(int delta) {
		Shot s = new Shot(this.x+32, this.y+32, this.damage);
		shots.add(s);
		if(facing == Facing.LEFT)
			s.shootLeft(delta);
		else
			s.shootRight(delta);
	}
	
	public ArrayList<Shot> getShots() {
		return shots;
	}
	
	public void removeShots(ArrayList<Shot> s) {
        shots.removeAll(s);
    }
}