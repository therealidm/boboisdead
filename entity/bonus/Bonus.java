package entity.bonus;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import entity.LevelObject;

public class Bonus extends LevelObject {

	protected Animation animation;

	public Bonus(float x, float y) {
		super(x, y);
	}
	
	public void render(float offsetX, float offsetY, Graphics g) {
        animation.draw(x-offsetX, y-offsetY);
    }
	
	protected void setAnimation(SpriteSheet spriteSheet, int frameDuration) {
        animation = new Animation(spriteSheet, frameDuration);
        animation.setPingPong(true);
	}
}