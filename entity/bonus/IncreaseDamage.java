package entity.bonus;

import main.Main;

public class IncreaseDamage extends Bonus {

	public IncreaseDamage(float x, float y) {
		super(x, y);
		setAnimation(Main.resources.getBonusDamage(), 125);
	}
}