package entity.bonus;

import main.Main;

public class IncreaseHealth extends Bonus {

	public IncreaseHealth(float x, float y) {
		super(x, y);
		setAnimation(Main.resources.getBonusHealth(), 125);
	}
}