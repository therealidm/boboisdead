package entity;

import main.Main;

import org.newdawn.slick.Image;

import entity.character.Player;

public class HUD {

	private Player player1 = null;
	private Player player2 = null;
	
	public HUD(Player player1) {
		this.player1 = player1;
	}
	
	public HUD(Player player1, Player player2) {
		this.player1 = player1;
		this.player2 = player2;
	}
	
	public void render() {
		healthToImage(player1.getHealth(), player1.getMaximumHealth()).draw(5, 5, 200, 50);
		if(Main.multiplayer)
			healthToImage(player2.getHealth(), player2.getMaximumHealth()).draw(800, 5, 200, 50);
	}
	
	public Image healthToImage(int health, int maxHealth) {
		float ratio = (float) health / (float) maxHealth;
		
		if(ratio <= 1.0 && ratio > 0.8)
			return Main.resources.getHuds().get(0);
		if(ratio <= 0.8 && ratio > 0.6)
			return Main.resources.getHuds().get(1);
		if(ratio <= 0.6 && ratio > 0.4)
			return Main.resources.getHuds().get(2);
		if(ratio <= 0.4 && ratio > 0.2)
			return Main.resources.getHuds().get(3);
		if(ratio <= 0.2 && ratio > 0.0)
			return Main.resources.getHuds().get(4);
		if(ratio <= 0)
			return Main.resources.getHuds().get(5);
		
		return null;
	}
}