package entity;

import java.util.HashMap;

import main.Main;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import physics.AABoundingRect;
import enums.Facing;

public class Shot extends LevelObject {

	private Facing facing;
	private HashMap<Facing,Image> sprites;
	
	private float accelerationSpeed = 0.5f;
	private float maximumSpeed = 0.5f;
	private int damage;
	private boolean remove;
	
	public Shot(float x, float y, int damage) {
		super(x, y);
		this.damage = damage;
		facing = Facing.RIGHT;
		
		sprites = new HashMap<Facing,Image>();
		sprites.put(Facing.RIGHT, Main.resources.getShots().get(Main.levelSelected));
		sprites.put(Facing.LEFT, Main.resources.getShots().get(Main.levelSelected).getFlippedCopy(true, false));
		
		boundingShape = new AABoundingRect(x, y, 20, 20);
	}

	public void render(float offsetX, float offsetY, Graphics g) {
		sprites.get(facing).draw(x-offsetX, y-offsetY);
	}
	
	public void shootLeft(int delta) {
		Main.resources.getSoundEffects().get(1).play();
		
        if(xVelocity > -maximumSpeed) {
            xVelocity -= accelerationSpeed*delta;
            if(xVelocity < -maximumSpeed) {
                xVelocity = -maximumSpeed;
            }
        }
        facing = Facing.LEFT;
    }
	
	public void shootRight(int delta) {
		Main.resources.getSoundEffects().get(1).play();
		
        if(xVelocity < maximumSpeed) {
            xVelocity += accelerationSpeed*delta;
            if(xVelocity > maximumSpeed) {
                xVelocity = maximumSpeed;
            }
        }
        facing = Facing.RIGHT;
    }
	
	public int getDamage() {
		return damage;
	}
	
	public boolean shouldRemove() {
		return remove;
	}
	
	public void setRemove(boolean remove) {
		this.remove = remove;
	}
}