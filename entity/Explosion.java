package entity;

import main.Main;

import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;

public class Explosion {
	
	float x;
	float y;
	long startTime;
	private ParticleSystem system;
	private boolean remove;

	public Explosion(float x, float y) {
		this.x = x;
		this.y = y;
		
		system = new ParticleSystem(Main.resources.getParticle(), 1500);
		
		try {
			ConfigurableEmitter emitter = ParticleIO.loadEmitter(Main.resources.getXmlFiles().get(Main.levelSelected));
			emitter.setPosition(this.x, this.y);
			system.addEmitter(emitter);
		} catch(Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		system.setBlendingMode(ParticleSystem.BLEND_ADDITIVE);
		
		startTime = System.currentTimeMillis();
	}
	
	public void render() {
		if(System.currentTimeMillis()-startTime < 500) {
			system.render();
		} else {
			remove = true;
		}
	}
	
	public void update(int delta) {
		system.update(delta);
	}
	
	public boolean shouldRemove() {
		return remove;
	}
}